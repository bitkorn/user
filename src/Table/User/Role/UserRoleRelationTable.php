<?php

namespace Bitkorn\User\Table\User\Role;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class UserRoleRelationTable extends AbstractLibTable
{
    /**
     * @var string
     */
    protected $table = 'user_role_relation';

    public function getUserRoleRelation(string $userUuid): string
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'user_uuid' => $userUuid,
            ]);
            $result = $this->selectWith($select);
            /** @var HydratingResultSet $result */
            if ($result->valid() && $result->count() > 0) {
                $current = $result->current();
                return $current['user_role_relation_uuid'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $userUuid
     * @param int $userRoleId
     * @return string
     */
    public function insertUserRoleRelation(string $userUuid, int $userRoleId): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'user_role_relation_uuid' => $uuid,
                'user_uuid'               => $userUuid,
                'user_role_id'            => $userRoleId
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $userRoleRelationUuid
     * @param int $userRoleId
     * @return int
     */
    public function updateUserRoleRelation(string $userRoleRelationUuid, int $userRoleId): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_role_id' => $userRoleId]);
            $update->where(['user_role_relation_uuid' => $userRoleRelationUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function deleteForUser(string $userUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_uuid' => $userUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userUuid
     * @param int $userRoleId
     * @return int
     */
    public function deleteUserRoleRelation(string $userUuid, int $userRoleId): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_uuid' => $userUuid, 'user_role_id' => $userRoleId]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
