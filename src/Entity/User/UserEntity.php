<?php

namespace Bitkorn\User\Entity\User;

use Bitkorn\User\Entity\User\Group\UserGroupEntity;
use Bitkorn\User\Entity\User\Right\UserRightRelationEntity;
use Bitkorn\User\Entity\User\Role\UserRoleSpecialRelationEntity;
use Bitkorn\User\Table\User\Group\UserGroupRelationTable;
use Bitkorn\User\Table\User\Right\UserRightRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Bitkorn\Trinket\Entity\AbstractEntity;
use Bitkorn\User\Entity\User\Role\UserRoleEntity;
use Bitkorn\User\Table\User\Role\UserRoleSpecialRelationTable;

/**
 * Class UserEntity
 *
 * @package Bitkorn\User\Entity\User
 */
class UserEntity extends AbstractEntity
{
    protected array $mapping = [
        'user_uuid'        => 'user_uuid',
        'user_login'       => 'user_login',
        'user_passwd'      => 'user_passwd',
        'user_active'      => 'user_active',
        'user_time_create' => 'user_time_create',
        'user_time_update' => 'user_time_update',
        'user_email'       => 'user_email',
        'user_lang_iso'    => 'user_lang_iso',
        // JOIN
        'user_role_id'     => 'user_role_id',
    ];

    protected int $userRole = 0;

    /**
     * @var array Array of user_group_alias
     */
    protected array $userGroups = [];

    /**
     * @var array Array of user_right_alias`s
     */
    protected array $userRights = [];

    protected UserRoleEntity $userRoleEntity;

    /**
     * @var UserRoleSpecialRelationEntity[] UserRoleSpecialAlias indexed array.
     */
    protected array $userRoleSpecialRelationEntities;

    /**
     * @var UserGroupEntity[] user_group.user_group_alias indexed array.
     */
    protected array $userGroupEntities = [];

    /**
     * @var UserRightRelationEntity[] user_right.user_right_alias indexed array
     */
    protected array $userRightRelationEntities = [];

    public function getUserRoleEntity(): UserRoleEntity
    {
        return $this->userRoleEntity;
    }

    public function getUserRole(): int
    {
        return $this->userRole;
    }

    /**
     * @return array Array of user_group_alias
     */
    public function getUserGroups(): array
    {
        return $this->userGroups;
    }

    /**
     * @return array Array of user_right_alias`s
     */
    public function getUserRights(): array
    {
        return $this->userRights;
    }

    /**
     * Login Step 1
     *
     * @param array $userData
     * @return bool
     */
    public function loadWithUserData(array $userData): bool
    {
        if (!$this->exchangeArrayFromDatabase($userData) || empty($this->storage['user_uuid'])) {
            return false;
        }
        $this->uuid = $this->storage['user_uuid'];

        return true;
    }

    /**
     * Login Step 2
     * @param UserRoleTable $userRoleTable
     * @return bool
     */
    public function loadUserRole(UserRoleTable $userRoleTable): bool
    {
        $this->userRoleEntity = new UserRoleEntity();
        $this->userRoleEntity->exchangeArrayFromDatabase($userRoleTable->getUserRoleForUser($this->uuid));
        $this->userRole = $this->userRoleEntity->getUserRoleId();
        return true;
    }

    /**
     * Login Step 3
     * @param UserRoleSpecialRelationTable $userRoleSpecialRelationTable
     * @return bool
     */
    public function loadUserRoleSpecialRelations(UserRoleSpecialRelationTable $userRoleSpecialRelationTable): bool
    {
        $userRoleSpecialRelations = $userRoleSpecialRelationTable->getUserRoleSpecialRelationsByRoleId($this->userRoleEntity->getUserRoleId());
        foreach ($userRoleSpecialRelations as $userRoleSpecialRelation) {
            $userRoleSpecialRelationEntity = new UserRoleSpecialRelationEntity();
            $userRoleSpecialRelationEntity->exchangeArrayFromDatabase($userRoleSpecialRelation);
            $this->userRoleSpecialRelationEntities[$userRoleSpecialRelationEntity->getUserRoleSpecialAlias()] = $userRoleSpecialRelationEntity;
        }
        return true;
    }

    /**
     * Login Step 4
     * @param UserRightRelationTable $userRightRelationTable
     * @return bool
     */
    public function loadUserRightRelations(UserRightRelationTable $userRightRelationTable): bool
    {
        $userRights = $userRightRelationTable->getUserRightRelationsByUserId($this->uuid);
        foreach ($userRights as $userRight) {
            $userRightRelationEntity = new UserRightRelationEntity();
            $userRightRelationEntity->exchangeArrayFromDatabase($userRight);
            $this->userRightRelationEntities[$userRightRelationEntity->getUserRightAlias()] = $userRightRelationEntity;
            $this->userRights[] = $userRightRelationEntity->getUserRightAlias();
        }
        return true;
    }

    /**
     * Login Step 5
     * @param UserGroupRelationTable $userGroupRelationTable
     * @return bool
     */
    public function loadUserGroupRelations(UserGroupRelationTable $userGroupRelationTable): bool
    {
        $userGroups = $userGroupRelationTable->getUserGroupsByUserUuid($this->uuid);
        foreach ($userGroups as $userGroup) {
            $userGroupEntity = new UserGroupEntity();
            $userGroupEntity->exchangeArrayFromDatabase($userGroup);
            $this->userGroupEntities[$userGroupEntity->getUserGroupAlias()] = $userGroupEntity;
            $this->userGroups[] = $userGroupEntity->getUserGroupAlias();
        }
        return true;
    }

    /**
     * @param int $userRoleId
     * @return bool
     */
    public function isUserInRole(int $userRoleId): bool
    {
        if (!isset($this->userRoleEntity)) {
            return false;
        }
        return $this->userRoleEntity->getUserRoleId() == $userRoleId;
    }

    /**
     * @param string $userRoleAlias
     * @return bool
     */
    public function isUserInRoleAlias(string $userRoleAlias): bool
    {
        if (!isset($this->userRoleEntity)) {
            return false;
        }
        return ($this->userRoleEntity->getUserRoleAlias() == $userRoleAlias);
    }

    /**
     * It behaves as on the winner's steps. One has the highest importance and is in all other roles in it.
     *
     * @param int $userRoleId
     * @return bool
     */
    public function isUserInRoleMin(int $userRoleId): bool
    {
        if (!isset($this->userRoleEntity)) {
            return false;
        }
        $roleId = $this->userRoleEntity->getUserRoleId();
        return $roleId > 0 && $roleId <= $userRoleId;
    }

    /**
     * @param string $userRoleSpecialAlias
     * @return bool
     */
    public function hasUserRoleSpecial(string $userRoleSpecialAlias): bool
    {
        return (isset($this->userRoleSpecialRelationEntities[$userRoleSpecialAlias]) && $this->userRoleSpecialRelationEntities[$userRoleSpecialAlias]->userRoleSpecialRelationEnabled());
    }

    /**
     * @param string $userGroupAlias
     * @return bool
     */
    public function isUserInGroup(string $userGroupAlias): bool
    {
        if (empty($this->userGroupEntities)) {
            return false;
        }
        return isset($this->userGroupEntities[$userGroupAlias]);
    }

    /**
     * @param string $userRightAlias
     * @return bool
     */
    public function hasUserRight(string $userRightAlias): bool
    {
        return (isset($this->userRightRelationEntities[$userRightAlias]) && $this->userRightRelationEntities[$userRightAlias]->userRightRelationEnabled());
    }

    /**
     * @return array
     */
    public function getRightsnRoles(): array
    {
        $rightsnRolesArray = [];
        $rightsnRolesArray['user_role_id'] = $this->userRoleEntity->getUserRoleId();
        $rightsnRolesArray['user_role_alias'] = $this->userRoleEntity->getUserRoleAlias();

        if (isset($this->userRoleSpecialRelationEntities) && !empty($this->userRoleSpecialRelationEntities)) {
            foreach ($this->userRoleSpecialRelationEntities as $userRoleSpecialRelationEntity) {
                if (!isset($rightsnRolesArray['user_role_specials'])) {
                    $rightsnRolesArray['user_role_specials'] = [];
                }
                $rightsnRolesArray['user_role_specials'][$userRoleSpecialRelationEntity->getUserRoleSpecialAlias()] = $userRoleSpecialRelationEntity->getUserRoleSpecialRelationValue();
            }
        }

        if (!empty($this->userRightRelationEntities)) {
            foreach ($this->userRightRelationEntities as $userRightRelationEntity) {
                if (!isset($rightsnRolesArray['user_rights'])) {
                    $rightsnRolesArray['user_rights'] = [];
                }
                $rightsnRolesArray['user_rights'][$userRightRelationEntity->getUserRightAlias()] = $userRightRelationEntity->getUserRightRelationValue();
            }
        }

        foreach ($this->userGroupEntities as $userGroupEntity) {
            if (!isset($rightsnRolesArray['user_groups'])) {
                $rightsnRolesArray['user_groups'] = [];
            }
            $rightsnRolesArray['user_groups'][$userGroupEntity->getUserGroupAlias()] = $userGroupEntity->getUserGroupName();
        }

        return $rightsnRolesArray;
    }

    public function getUserLang(): string
    {
        if (!isset($this->storage['user_lang_iso'])) {
            return '';
        }
        return $this->storage['user_lang_iso'];
    }

    /**
     * @return int
     */
    public function getUserTimeCreate(): int
    {
        if (!isset($this->storage['user_time_create'])) {
            return 0;
        }
        return intval($this->storage['user_time_create']);
    }

    public function getUserLogin(): string
    {
        if (!isset($this->storage['user_login'])) {
            return '';
        }
        return $this->storage['user_login'];
    }

    public function getUserEmail(): string
    {
        if (!isset($this->storage['user_email'])) {
            return '';
        }
        return $this->storage['user_email'];
    }

    public function getUserLangIso(): string
    {
        if (!isset($this->storage['user_lang_iso'])) {
            return '';
        }
        return $this->storage['user_lang_iso'];
    }
}
