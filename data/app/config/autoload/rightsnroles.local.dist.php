<?php
/**
 * Either you set the array key 'rightsnroles' in the respective route, or create a file like here and put it in the autoload folder.
 */

use Bitkorn\User\Entity\User\Rightsnroles;

return [
    'router' => [
        'routes' => [
            'bitkorn_user_ajax_lists_user'                  => [
                'rightsnroles' => [
                    Rightsnroles::KEY_DESC    => 'A description can help to identify the route ;)',
                    Rightsnroles::KEY_ROLEMIN => 4,
                ]
            ],
            'bitkorn_user_rest_user_user'                   => [
                'rightsnroles' => [
                    Rightsnroles::KEY_DESC => 'REST methods must have a longer description because they are five request methods',
                    /**
                     * For each implemented REST method one configuration:
                     */
                    Rightsnroles::KEY_REST => [
                        Rightsnroles::METHOD_GET    => [
                            Rightsnroles::KEY_ROLEMIN => 5,
                        ],
                        Rightsnroles::METHOD_GETP   => [
                            Rightsnroles::KEY_ROLEMIN => 5,
                        ],
                        Rightsnroles::METHOD_POST   => [
                            Rightsnroles::KEY_ROLEMIN => 3,
                        ],
                        Rightsnroles::METHOD_UPDATE => [
                            Rightsnroles::KEY_ROLEMIN => 4,
                            Rightsnroles::KEY_GROUPS  => [
                                'project_manager'
                            ],
                            Rightsnroles::KEY_RIGHTS  => [6, 73],
                        ],
                        Rightsnroles::METHOD_DELETE => [
                            Rightsnroles::KEY_ROLE => 1,
                        ],
                    ],
                ],
            ],
            //
            'bitkorn_user_ajax_access_authentication_check' => [
                'rightsnroles' => [
                    Rightsnroles::KEY_DESC    => 'AJAX check for authentication',
                    Rightsnroles::KEY_ROLEMIN => 5,
                ]
            ],
        ],
    ]
];
