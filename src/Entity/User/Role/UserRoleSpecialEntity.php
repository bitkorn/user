<?php

namespace Bitkorn\User\Entity\User\Role;


use Bitkorn\Trinket\Entity\AbstractEntity;

class UserRoleSpecialEntity extends AbstractEntity
{

    protected array $mapping = [
        'user_role_special_uuid' => 'user_role_special_uuid',
        'user_role_special_alias' => 'user_role_special_alias',
        'user_role_special_name' => 'user_role_special_name',
        'user_role_special_desc' => 'user_role_special_desc',
        'user_role_special_time_create' => 'user_role_special_time_create',
        'user_role_special_order_priority' => 'user_role_special_order_priority',
    ];

    public function getUserRoleSpecialAlias(): string
    {
        if (!isset($this->storage['user_role_special_alias'])) {
            return '';
        }
        return $this->storage['user_role_special_alias'];
    }

    public function getUserRoleSpecialDesc(): string
    {
        if (!isset($this->storage['user_role_special_desc'])) {
            return '';
        }
        return $this->storage['user_role_special_desc'];
    }

}
