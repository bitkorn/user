<?php

namespace Bitkorn\User\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\User\Service\RightsnrolesRoutesService;
use Bitkorn\User\Service\Role\UserRoleService;
use Laminas\Http\Response;

class ListsController extends AbstractUserController
{
    protected UserRoleService $userRoleService;
    protected RightsnrolesRoutesService $rightsnrolesService;

    public function setUserRoleService(UserRoleService $userRoleService): void
    {
        $this->userRoleService = $userRoleService;
    }

    public function setRightsnrolesService(RightsnrolesRoutesService $rightsnrolesService): void
    {
        $this->rightsnrolesService = $rightsnrolesService;
    }

    /**
     * @return JsonModel
     */
    public function userUuidAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $onlyActive = $this->params('only_active', 0) == 1;
        $asKeyValObj = filter_var($this->params()->fromQuery('asKeyValObj', false), FILTER_VALIDATE_BOOLEAN);
        if ($asKeyValObj) {
            $jsonModel->setKeyValObjArr($this->userService->getUserUuidAssoc($onlyActive, $asKeyValObj));
        } else {
            $jsonModel->setObj($this->userService->getUserUuidAssoc($onlyActive, $asKeyValObj));
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function userRolesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->userRoleService->getUserRoles());
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function rightsnrolesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserContainer()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->rightsnrolesService->getRightsnroles());
        return $jsonModel;
    }
}
