<?php

namespace Bitkorn\User\Entity\User;

use Bitkorn\Trinket\Entity\AbstractEntity;

class UserFormEntity extends AbstractEntity
{
    public array $mapping = [
        'user_uuid'           => 'user_uuid',
        'user_login'          => 'user_login',
        'user_active'         => 'user_active',
        'user_email'          => 'user_email',
        'user_lang_iso'       => 'user_lang_iso',
    ];

    protected $primaryKey = 'user_uuid';

    public function getUserUuid(): string
    {
        if (!isset($this->storage['user_uuid'])) {
            return '';
        }
        return $this->storage['user_uuid'];
    }

    public function setUserUuid(string $userUuid): void
    {
        $this->storage['user_uuid'] = $userUuid;
    }

    public function getUserLogin(): string
    {
        if (!isset($this->storage['user_login'])) {
            return '';
        }
        return $this->storage['user_login'];
    }

    public function setUserLogin(string $userLogin): void
    {
        $this->storage['user_login'] = $userLogin;
    }

    public function getUserActive(): int
    {
        if (!isset($this->storage['user_active'])) {
            return 0;
        }
        return $this->storage['user_active'];
    }

    public function setUserActive(int $userActive): void
    {
        $this->storage['user_active'] = $userActive;
    }

    public function getUserEmail(): string
    {
        if (!isset($this->storage['user_email'])) {
            return '';
        }
        return $this->storage['user_email'];
    }

    public function setUserEmail(string $userEmail): void
    {
        $this->storage['user_email'] = $userEmail;
    }

    public function getUserLangIso(): string
    {
        if (!isset($this->storage['user_lang_iso'])) {
            return '';
        }
        return $this->storage['user_lang_iso'];
    }

    public function setUserLangIso(string $userLangIso): void
    {
        $this->storage['user_lang_iso'] = $userLangIso;
    }
}
