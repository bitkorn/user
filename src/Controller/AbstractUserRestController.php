<?php

namespace Bitkorn\User\Controller;

use Bitkorn\User\Service\UserService;
use Bitkorn\Trinket\Controller\AbstractRestController;

class AbstractUserRestController extends AbstractRestController
{

    protected UserService $userService;

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }
}
