<?php

namespace Bitkorn\User;

use Bitkorn\User\Entity\User\Rightsnroles;
use Bitkorn\User\Service\UserService;
use Laminas\EventManager\EventInterface;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\ModuleManager\Feature\BootstrapListenerInterface;
use Laminas\Mvc\MvcEvent;

class Module implements ConfigProviderInterface, BootstrapListenerInterface
{

    public function getConfig(): array
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * Listen to the bootstrap event
     *
     * @param EventInterface $e
     * @return void
     */
    public function onBootstrap(EventInterface $e)
    {
        if (!$e instanceof MvcEvent) {
            return;
        }
        $app = $e->getApplication();
        $eventManager = $app->getEventManager();
        $services = $app->getServiceManager();

        $eventManager->attach($e::EVENT_DISPATCH, function (MvcEvent $e) use ($services, $app) {
            $routeName = $e->getRouteMatch()->getMatchedRouteName();
            $routes = $services->get('config')['router']['routes'];
            if (!isset($routes[$routeName]) || !isset($routes[$routeName]['rightsnroles']) || empty($routes[$routeName]['rightsnroles'])) {
                return;
            }
            $rr = new Rightsnroles($routes[$routeName]['rightsnroles']);
            /** @var UserService $userService */
            $userService = $services->get(UserService::class);
            $userService->setRightsnroles($rr);
        }, 2);
    }
}
