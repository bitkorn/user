<?php

namespace Bitkorn\User\Table\User\Role;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class UserRoleSpecialTable extends AbstractLibTable
{
    protected $table = 'user_role_special';

    /**
     * @return array
     */
    public function getUserRoleSpecials(): array
    {
        $select = $this->sql->select();
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $alias
     * @return bool
     */
    public function existAlias(string $alias): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_role_special_alias' => $alias]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $roleSpecialAlias
     * @param string $roleSpecialDesc
     * @return string
     */
    public function insertUserRoleSpecial(string $roleSpecialAlias, string $roleSpecialDesc): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'user_role_special_uuid'        => $uuid,
                'user_role_special_alias'       => $roleSpecialAlias,
                'user_role_special_desc'        => $roleSpecialDesc,
                'user_role_special_time_create' => time()
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $roleSpecialUuid
     * @return int
     */
    public function deleteUserRoleSpecial(string $roleSpecialUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_role_special_uuid' => $roleSpecialUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
