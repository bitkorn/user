<?php

namespace Bitkorn\User\Factory\Controller\Ajax;

use Bitkorn\User\Controller\Ajax\ListsController;
use Bitkorn\User\Service\RightsnrolesRoutesService;
use Bitkorn\User\Service\Role\UserRoleService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ListsControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ListsController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setUserRoleService($container->get(UserRoleService::class));
        $controller->setRightsnrolesService($container->get(RightsnrolesRoutesService::class));
        return $controller;
    }
}
