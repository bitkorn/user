<?php

namespace Bitkorn\User\Factory\Service\Right;

use Bitkorn\User\Service\Right\UserRightService;
use Bitkorn\User\Table\User\Right\UserRightRelationTable;
use Bitkorn\User\Table\User\Right\UserRightTable;
use Bitkorn\User\Table\User\ViewUserRightRelationTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserRightServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserRightService();
        $service->setLogger($container->get('logger'));
        $service->setUserRightTable($container->get(UserRightTable::class));
        $service->setUserRightRelationTable($container->get(UserRightRelationTable::class));
        $service->setViewUserRightRelationTable($container->get(ViewUserRightRelationTable::class));
        return $service;
    }
}
