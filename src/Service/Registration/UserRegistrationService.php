<?php

namespace Bitkorn\User\Service\Registration;

use Bitkorn\User\Service\UserService;
use Bitkorn\User\Table\User\UserTable;
use Bitkorn\Mail\Mail\Draft\DraftManager;
use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\View\Helper\ServerUrl;
use Laminas\View\Helper\Url;

class UserRegistrationService extends AbstractService
{

    const TEMPLATE_EMAIL_VERIFY = 'template/email/registration/email-verify';

    protected array $userConfig;
    protected UserTable $userTable;
    protected MailWrapper $mailWrapper;
    protected DraftManager $draftManager;
    protected Url $urlHelper;
    protected ServerUrl $serverUrlHelper;
    protected UserService $userService;
    protected string $routeVerificationEmail;

    public function setUserConfig(array $userConfig): void
    {
        $this->userConfig = $userConfig;
        $this->routeVerificationEmail = $userConfig['registration_verify_route'];
    }

    public function setUserTable(UserTable $userTable): void
    {
        $this->userTable = $userTable;
    }

    public function setMailWrapper(MailWrapper $mailWrapper): void
    {
        $this->mailWrapper = $mailWrapper;
    }

    public function setDraftManager(DraftManager $draftManager): void
    {
        $this->draftManager = $draftManager;
    }

    public function setUrlHelper(Url $urlHelper): void
    {
        $this->urlHelper = $urlHelper;
    }

    public function setServerUrlHelper(ServerUrl $serverUrlHelper): void
    {
        $this->serverUrlHelper = $serverUrlHelper;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function setRouteVerificationEmail(string $routeVerificationEmail): void
    {
        $this->routeVerificationEmail = $routeVerificationEmail;
    }

    /**
     * @return string
     */
    private function getCode(): string
    {
        $code = substr($this->getHash(), 3, 10);
        if ($this->userTable->existPasswdForgotHash($code)) {
            return $this->getCode();
        }
        return $code;
    }

    /**
     * @return string
     */
    private function getHash(): string
    {
        $hash = hash('sha256', 'saltLikeCity:)' . time() . mt_rand(10000, 99999));
        if ($this->userTable->existPasswdForgotHash($hash)) {
            return $this->getCode();
        }
        return $hash;
    }

    /**
     * @param array $userData [
     *  'user_role_id' => 5,
     *  'user_login' => $login,
     *  'user_passwd' => $passwd1,
     *  'user_passwd_confirm' => $passwd2,
     *  'user_active' => 0,
     *  'user_email' => $email,
     *  'user_lang_iso' => $lang
     * ]
     * @return bool
     */
    public function registerUser(array $userData): bool
    {
        if (empty($userUuid = $this->userService->createNewUser($userData))) {
            return false;
        }
        if (!$this->sendVerificationEmail($userUuid, $userData['user_email'], $userData['user_login'])) {
            $this->userService->deleteUser($userUuid);
            return false;
        }
        return true;
    }

    /**
     * @param string $userUuid
     * @param string $emailTarget
     * @param string $username
     * @return bool
     */
    protected function sendVerificationEmail(string $userUuid, string $emailTarget, string $username): bool
    {
        $hash = $this->getHash();
        if ($this->userTable->updateRegistrationHash($userUuid, $hash) != 1) {
            return false;
        }

        $this->draftManager->setTemplate('template/email/registration/email-verify');
        $url = call_user_func($this->serverUrlHelper, call_user_func($this->urlHelper, $this->routeVerificationEmail));
        $this->draftManager->setViewVariable('verifyUrl', $url . '?hash=' . $hash);
        $this->draftManager->setViewVariable('username', $username);

        $this->mailWrapper->setSubject('Please verify your Emailaddress');
        $this->mailWrapper->setFromName($this->userConfig['brand_name']);
        $this->mailWrapper->setFromEmail($this->userConfig['brand_email']);
        $this->mailWrapper->setToName($username);
        $this->mailWrapper->setToEmail($emailTarget);
        $this->mailWrapper->setTextHtml($this->draftManager->render());
        try {
            return $this->mailWrapper->sendMail() == 1;
        } catch (\Exception $exception) {
            $this->logger->err($exception->getMessage());
            $this->logger->err($exception->getFile());
            $this->logger->err($exception->getLine());
        }
        return false;
    }

    /**
     * @param string $registrationHash
     * @return bool
     */
    public function verifyRegistration(string $registrationHash): bool
    {
        return $this->userTable->verifyRegistration($registrationHash);
    }

    /**
     * @param string $userLogin
     * @param string $userEmail
     * @return bool
     */
    public function passwordForgotSendCode(string $userLogin, string $userEmail): bool
    {
        if (!empty($userLogin)) {
            $userData = $this->userTable->getUserByLogin($userLogin);
        } else if (!empty($userEmail)) {
            $userData = $this->userTable->getUserByEmail($userEmail);
        }
        if (empty($userData['user_uuid'])) {
            return false;
        }
        $code = $this->getCode();

        if ($this->userTable->updatePasswordForgotHash($userData['user_uuid'], $code) != 1) {
            return false;
        }
        $this->draftManager->setTemplate('template/email/registration/password-forgot-code');
        $this->draftManager->setViewVariable('username', $userData['user_login']);
        $this->draftManager->setViewVariable('code', $code);

        $this->mailWrapper->setSubject($this->userConfig['brand_name'] . ' - code to renew your password');
        $this->mailWrapper->setFromName($this->userConfig['brand_name']);
        $this->mailWrapper->setFromEmail($this->userConfig['brand_email']);
        $this->mailWrapper->setToName($userData['user_login']);
        $this->mailWrapper->setToEmail($userData['user_email']);
        $this->mailWrapper->setTextHtml($this->draftManager->render());
        try {
            return $this->mailWrapper->sendMail() == 1;
        } catch (\Exception $exception) {
            $this->logger->err($exception->getMessage());
            $this->logger->err($exception->getFile());
            $this->logger->err($exception->getLine());
        }
        return false;
    }

    /**
     * @param string $userLogin
     * @param string $userEmail
     * @return bool
     */
    public function passwordForgot(string $userLogin, string $userEmail): bool
    {
        if (!empty($userLogin)) {
            $userData = $this->userTable->getUserByLogin($userLogin);
        } else if (!empty($userEmail)) {
            $userData = $this->userTable->getUserByEmail($userEmail);
        }
        if (empty($userData['user_uuid'])) {
            return false;
        }
        $hash = $this->getHash();

        if ($this->userTable->updatePasswordForgotHash($userData['user_uuid'], $hash) != 1) {
            return false;
        }
        $this->draftManager->setTemplate('template/email/registration/password-forgot');
        $url = call_user_func($this->serverUrlHelper, call_user_func($this->urlHelper, 'bitkorn_user_ajax_access_registration_passwordrenew'));
        $this->draftManager->setViewVariable('renewPasswdUrl', $url . '?hash=' . $hash);
        $this->draftManager->setViewVariable('username', $userData['user_login']);

        $this->mailWrapper->setSubject('Now you can update your password');
        $this->mailWrapper->setFromName($this->userConfig['brand_name']);
        $this->mailWrapper->setFromEmail($this->userConfig['brand_email']);
        $this->mailWrapper->setToName($userData['user_login']);
        $this->mailWrapper->setToEmail($userData['user_email']);
        $this->mailWrapper->setTextHtml($this->draftManager->render());
        try {
            return $this->mailWrapper->sendMail() == 1;
        } catch (\Exception $exception) {
            $this->logger->err($exception->getMessage());
            $this->logger->err($exception->getFile());
            $this->logger->err($exception->getLine());
        }
        return false;
    }

}
