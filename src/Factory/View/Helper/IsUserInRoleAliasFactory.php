<?php

namespace Bitkorn\User\Factory\View\Helper;

use Bitkorn\User\Service\UserService;
use Bitkorn\User\View\Helper\IsUserInRoleAlias;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class IsUserInRoleAliasFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new IsUserInRoleAlias();
        $viewHelper->setLogger($container->get('logger'));
        $viewHelper->setUserService($container->get(UserService::class));
        return $viewHelper;
    }
}
