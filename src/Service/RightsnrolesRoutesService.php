<?php

namespace Bitkorn\User\Service;

use Bitkorn\Trinket\Service\AbstractService;

class RightsnrolesRoutesService extends AbstractService
{
    protected array $routes;

    public function setRoutes(array $routes): void
    {
        $this->routes = $routes;
    }

    public function getRightsnroles(): array
    {
        $rnr = [];
        foreach ($this->routes as $name => $route) {
            if (!isset($route['rightsnroles'])) {
                continue;
            }
            $rnr[] = [
                'name'         => $name,
                'route'        => $route['options']['route'],
                'rightsnroles' => $route['rightsnroles']
            ];
        }
        return $rnr;
    }
}
