<?php

namespace Bitkorn\User\Factory\Table\User;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\User\Table\User\UserTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserTableFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $table = new UserTable();
        $table->setDbAdapter($container->get('dbDefault'));
        $table->setLogger($container->get('logger'));
        return $table;
    }
}
