<?php

namespace Bitkorn\User\Entity\User;

/**
 * Class Rightsnroles
 * @package Bitkorn\User\Entity\User
 * This is a mapper class for route access role, groups and rights.
 * It has a function to check some roles, groups and rights.
 * It enables the configuration of roles, groups and rights in module external files
 */
class Rightsnroles
{
    /**
     * A description for the route
     */
    const KEY_DESC = 'desc';

    /**
     * The REST resource array?
     */
    const KEY_REST = 'rest';

    /**
     * Role that must match. 0 means no effect.
     */
    const KEY_ROLE = 'role';

    /**
     * Role that must be at least. God=1, others >= 2
     */
    const KEY_ROLEMIN = 'rolemin';

    /**
     * A list with user_group_alias
     */
    const KEY_GROUPS = 'groups';

    /**
     * A list with user_right_ids - perhaps in the future: assoc [user_right_id => user_right_relation_value]
     */
    const KEY_RIGHTS = 'rights';

    const METHOD_GET = 'GET';
    const METHOD_GETP = 'GETp';
    const METHOD_POST = 'POST';
    const METHOD_UPDATE = 'UPDATE';
    const METHOD_DELETE = 'DELETE';

    protected int $role = 0;
    protected int $roleMin = 0;
    protected array $groups = [];
    protected array $rights = [];

    protected bool $rest = false;
    protected int $getRole = 0;
    protected int $getRoleMin = 0;
    protected array $getGroups = [];
    protected array $getRights = [];
    protected int $getpRole = 0;
    protected int $getpRoleMin = 0;
    protected array $getpGroups = [];
    protected array $getpRights = [];
    protected int $postRole = 0;
    protected int $postRoleMin = 0;
    protected array $postGroups = [];
    protected array $postRights = [];
    protected int $updateRole = 0;
    protected int $updateRoleMin = 0;
    protected array $updateGroups = [];
    protected array $updateRights = [];
    protected int $deleteRole = 0;
    protected int $deleteRoleMin = 0;
    protected array $deleteGroups = [];
    protected array $deleteRights = [];

    /**
     * Rightsnroles constructor.
     * Take a 'routes' array and put it into this constructor.
     * @param array $rightsnroles Rightsnroles array from module.config.php[router][routes]["route_name_unique"][rightsnroles]
     */
    public function __construct(array $rightsnroles)
    {
        $this->init($rightsnroles);
    }

    /**
     * Use this function to overwrite existing rightsnroles;
     * @param array $r The rightsnroles.
     */
    protected function init(array $r): void
    {
        if (!empty($r[self::KEY_REST]) && is_array($r[self::KEY_REST])) {
            $this->rest = true;
            $this->parseRest($r[self::KEY_REST]);
        } else {
            $this->role = !empty($r[self::KEY_ROLE]) ? $r[self::KEY_ROLE] : 0;
            $this->roleMin = !empty($r[self::KEY_ROLEMIN]) ? $r[self::KEY_ROLEMIN] : 0;
            if (!empty($r[self::KEY_GROUPS]) && is_array($r[self::KEY_GROUPS])) {
                foreach ($r[self::KEY_GROUPS] as $group) {
                    $this->groups[] = $group;
                }
            }
            if (!empty($r[self::KEY_RIGHTS]) && is_array($r[self::KEY_RIGHTS])) {
                foreach ($r[self::KEY_RIGHTS] as $right) {
                    $this->rights[] = $right;
                }
            }
        }
    }

    protected function parseRest(array $rest): void
    {
        if (!empty($rest[self::METHOD_GET]) && is_array($rest[self::METHOD_GET])) {
            $m = $rest[self::METHOD_GET];
            $this->getRole = !empty($m[self::KEY_ROLE]) ? $m[self::KEY_ROLE] : 0;
            $this->getRoleMin = !empty($m[self::KEY_ROLEMIN]) ? $m[self::KEY_ROLEMIN] : 0;
            if (!empty($m[self::KEY_GROUPS]) && is_array($m[self::KEY_GROUPS])) {
                foreach ($m[self::KEY_GROUPS] as $group) {
                    $this->getGroups[] = $group;
                }
            }
            if (!empty($m[self::KEY_RIGHTS]) && is_array($m[self::KEY_RIGHTS])) {
                foreach ($m[self::KEY_RIGHTS] as $right) {
                    $this->getRights[] = $right;
                }
            }
        }
        if (!empty($rest[self::METHOD_GETP]) && is_array($rest[self::METHOD_GETP])) {
            $m = $rest[self::METHOD_GETP];
            $this->getpRole = !empty($m[self::KEY_ROLE]) ? $m[self::KEY_ROLE] : 0;
            $this->getpRoleMin = !empty($m[self::KEY_ROLEMIN]) ? $m[self::KEY_ROLEMIN] : 0;
            if (!empty($m[self::KEY_GROUPS]) && is_array($m[self::KEY_GROUPS])) {
                foreach ($m[self::KEY_GROUPS] as $group) {
                    $this->getpGroups[] = $group;
                }
            }
            if (!empty($m[self::KEY_RIGHTS]) && is_array($m[self::KEY_RIGHTS])) {
                foreach ($m[self::KEY_RIGHTS] as $right) {
                    $this->getpRights[] = $right;
                }
            }
        }
        if (!empty($rest[self::METHOD_POST]) && is_array($rest[self::METHOD_POST])) {
            $m = $rest[self::METHOD_POST];
            $this->postRole = !empty($m[self::KEY_ROLE]) ? $m[self::KEY_ROLE] : 0;
            $this->postRoleMin = !empty($m[self::KEY_ROLEMIN]) ? $m[self::KEY_ROLEMIN] : 0;
            if (!empty($m[self::KEY_GROUPS]) && is_array($m[self::KEY_GROUPS])) {
                foreach ($m[self::KEY_GROUPS] as $group) {
                    $this->postGroups[] = $group;
                }
            }
            if (!empty($m[self::KEY_RIGHTS]) && is_array($m[self::KEY_RIGHTS])) {
                foreach ($m[self::KEY_RIGHTS] as $right) {
                    $this->postRights[] = $right;
                }
            }
        }
        if (!empty($rest[self::METHOD_UPDATE]) && is_array($rest[self::METHOD_UPDATE])) {
            $m = $rest[self::METHOD_UPDATE];
            $this->updateRole = !empty($m[self::KEY_ROLE]) ? $m[self::KEY_ROLE] : 0;
            $this->updateRoleMin = !empty($m[self::KEY_ROLEMIN]) ? $m[self::KEY_ROLEMIN] : 0;
            if (!empty($m[self::KEY_GROUPS]) && is_array($m[self::KEY_GROUPS])) {
                foreach ($m[self::KEY_GROUPS] as $group) {
                    $this->updateGroups[] = $group;
                }
            }
            if (!empty($m[self::KEY_RIGHTS]) && is_array($m[self::KEY_RIGHTS])) {
                foreach ($m[self::KEY_RIGHTS] as $right) {
                    $this->updateRights[] = $right;
                }
            }
        }
        if (!empty($rest[self::METHOD_DELETE]) && is_array($rest[self::METHOD_DELETE])) {
            $m = $rest[self::METHOD_DELETE];
            $this->deleteRole = !empty($m[self::KEY_ROLE]) ? $m[self::KEY_ROLE] : 0;
            $this->deleteRoleMin = !empty($m[self::KEY_ROLEMIN]) ? $m[self::KEY_ROLEMIN] : 0;
            if (!empty($m[self::KEY_GROUPS]) && is_array($m[self::KEY_GROUPS])) {
                foreach ($m[self::KEY_GROUPS] as $group) {
                    $this->deleteGroups[] = $group;
                }
            }
            if (!empty($m[self::KEY_RIGHTS]) && is_array($m[self::KEY_RIGHTS])) {
                foreach ($m[self::KEY_RIGHTS] as $right) {
                    $this->deleteRights[] = $right;
                }
            }
        }
    }

    /**
     * @param string $method If REST then $method is required
     * @param int $role The role is also roleMin
     * @param array $groups Users groups.
     * @param array $rights Users rights.
     * @return bool
     * Use this function to check user access against roles, groups and rights.
     */
    public function hasAccess(string $method = '', int $role = 0, array $groups = [], array $rights = []): bool
    {
        if (!$this->rest) {
            if (
                ($this->role > 0 && $role > 0 && $role == $this->role)
                || ($this->roleMin > 0 && $role > 0 && $role <= $this->roleMin)
                || !empty($this->groups) && !empty($groups) && !empty(array_intersect($this->groups, $groups))
                || !empty($this->rights) && !empty($rights) && !empty(array_intersect($this->rights, $rights))
            ) {
                return true;
            }
        } else {
            switch ($method) {
                case self::METHOD_GET:
                    if (
                        ($this->getRole > 0 && $role > 0 && $role == $this->getRole)
                        || ($this->getRoleMin > 0 && $role > 0 && $role <= $this->getRoleMin)
                        || !empty($this->getGroups) && !empty($groups) && !empty(array_intersect($this->getGroups, $groups))
                        || !empty($this->getRights) && !empty($rights) && !empty(array_intersect($this->getRights, $groups))
                    ) {
                        return true;
                    }
                    break;
                case self::METHOD_GETP:
                    if (
                        ($this->getpRole > 0 && $role > 0 && $role == $this->getpRole)
                        || ($this->getpRoleMin > 0 && $role > 0 && $role <= $this->getpRoleMin)
                        || !empty($this->getpGroups) && !empty($groups) && !empty(array_intersect($this->getpGroups, $groups))
                        || !empty($this->getpRights) && !empty($rights) && !empty(array_intersect($this->getpRights, $groups))
                    ) {
                        return true;
                    }
                    break;
                case self::METHOD_POST:
                    if (
                        ($this->postRole > 0 && $role > 0 && $role == $this->postRole)
                        || ($this->postRoleMin > 0 && $role > 0 && $role <= $this->postRoleMin)
                        || !empty($this->postGroups) && !empty($groups) && !empty(array_intersect($this->postGroups, $groups))
                        || !empty($this->postRights) && !empty($rights) && !empty(array_intersect($this->postRights, $groups))
                    ) {
                        return true;
                    }
                    break;
                case self::METHOD_UPDATE:
                    if (
                        ($this->updateRole > 0 && $role > 0 && $role == $this->updateRole)
                        || ($this->updateRoleMin > 0 && $role > 0 && $role <= $this->updateRoleMin)
                        || !empty($this->updateGroups) && !empty($groups) && !empty(array_intersect($this->updateGroups, $groups))
                        || !empty($this->updateRights) && !empty($rights) && !empty(array_intersect($this->updateRights, $groups))
                    ) {
                        return true;
                    }
                    break;
                case self::METHOD_DELETE:
                    if (
                        ($this->deleteRole > 0 && $role > 0 && $role == $this->deleteRole)
                        || ($this->deleteRoleMin > 0 && $role > 0 && $role <= $this->deleteRoleMin)
                        || !empty($this->deleteGroups) && !empty($groups) && !empty(array_intersect($this->deleteGroups, $groups))
                        || !empty($this->deleteRights) && !empty($rights) && !empty(array_intersect($this->deleteRights, $groups))
                    ) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }
}
