<?php

namespace Bitkorn\User\Service\Role;

use Bitkorn\User\Table\User\Role\UserRoleRelationTable;
use Bitkorn\Trinket\Service\AbstractService;

class UserRoleRelationService extends AbstractService
{

    protected UserRoleRelationTable $userRoleRelationTable;

    public function setUserRoleRelationTable(UserRoleRelationTable $userRoleRelationTable): void
    {
        $this->userRoleRelationTable = $userRoleRelationTable;
    }

    /**
     * @param string $userUuid
     * @param int $userRoleId
     * @return string The new userRoleRelation UUID.
     */
    public function insertOrUpdateUserRoleRelation(string $userUuid, int $userRoleId): string
    {
        if (!empty($userRoleRelationUuid = $this->userRoleRelationTable->getUserRoleRelation($userUuid))) {
            if ($this->userRoleRelationTable->updateUserRoleRelation($userRoleRelationUuid, $userRoleId) < 0) {
                return '';
            }
            return $userRoleRelationUuid;
        }
        return $this->userRoleRelationTable->insertUserRoleRelation($userUuid, $userRoleId);
    }
}
