<?php

namespace Bitkorn\User\Controller\Admin\Ajax;

use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class UserManagementController extends AbstractUserController
{

    public function massDeleteAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $request = $this->getRequest();
        if ($request instanceof Request && $request->isPost()) {
            $userUuids = $request->getPost('userUuids');
            if (!is_array($userUuids) || empty($userUuids)) {
                return $jsonModel;
            }
            foreach ($userUuids as $userUuid) {
                if (!$this->userService->deleteUser($userUuid)) {
                    $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
                    return $jsonModel;
                }
            }
            $jsonModel->setSuccess(1);
        }

        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function switchActiveAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $request = $this->getRequest();
        $userUuid = $this->params('id');
        if (empty($userUuid) || !(new Uuid())->isValid($userUuid) || !$request instanceof Request || !$request->isGet()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if (!$this->userService->switchUserActive($userUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
