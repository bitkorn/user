<?php

namespace Bitkorn\User\Form\User;

use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\User\Validator\UsernameValidator;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Filter\ToInt;
use Laminas\Validator\EmailAddress;
use Laminas\Validator\Hostname;
use Laminas\Validator\Identical;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class UserForm extends AbstractForm implements InputFilterProviderInterface
{

    private bool $userPasswdAvailable = false;
    private bool $userRoleIdAvailable = false;
    private array $userRoleIdAssoc = [];
    private array $supportedLanguages = [];

    public function setUserPasswdAvailable(bool $userPasswdAvailable): void
    {
        $this->userPasswdAvailable = $userPasswdAvailable;
    }

    public function setUserRoleIdAvailable(bool $userRoleIdAvailable): void
    {
        $this->userRoleIdAvailable = $userRoleIdAvailable;
    }

    public function setUserRoleIdAssoc(array $userRoleIdAssoc): void
    {
        $this->userRoleIdAssoc = $userRoleIdAssoc;
    }

    public function setSupportedLanguages(array $supportedLanguages): void
    {
        $this->supportedLanguages = $supportedLanguages;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'user_uuid']);
        }
        if ($this->userRoleIdAvailable) {
            $this->add(['name' => 'user_role_id']);
        }
        $this->add(['name' => 'user_login']);
        if ($this->userPasswdAvailable) {
            $this->add(['name' => 'user_passwd']);
            $this->add(['name' => 'user_passwd_confirm']);
        }
        $this->add(['name' => 'user_active']);
        $this->add(['name' => 'user_email']);
        $this->add(['name' => 'user_lang_iso']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['user_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => StripTags::class],
                    ['name' => StringTrim::class]
                ], 'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }

        if ($this->userRoleIdAvailable) {
            $filter['user_role_id'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => StringTrim::class]
                ], 'validators' => [
                    [
                        'name'    => InArray::class,
                        'options' => [
                            'haystack' => array_keys($this->userRoleIdAssoc)
                        ]
                    ]
                ]
            ];
        }

        $filter['user_login'] = [
            'required'   => true,
            'filters'    => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name'    => UsernameValidator::class,
                    'options' => [
                        'break_chain_on_failure' => true
                    ]
                ],
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 3,
                        'max'      => 30,
                    ]
                ]
            ]
        ];

        if ($this->userPasswdAvailable) {
            $filter['user_passwd'] = [
                'required'   => true,
                'filters'    => [
                    ['name' => StringTrim::class]
                ],
                'validators' => [
                    [
                        'name'    => StringLength::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min'      => 8,
                            'max'      => 100,
                        ]
                    ]
                ]
            ];

            $filter['user_passwd_confirm'] = [
                'required'   => true,
                'filters'    => [
                    ['name' => StringTrim::class]
                ],
                'validators' => [
                    [
                        'name'    => Identical::class,
                        'options' => [
                            'token' => 'user_passwd'
                        ]
                    ]
                ]
            ];
        }

        $filter['user_active'] = [
            'required' => true,
            'filters'  => [
                ['name' => ToInt::class]
            ]
        ];

        $filter['user_email'] = [
            'required'   => true,
            'validators' => [
                [
                    'name'    => EmailAddress::class,
                    'options' => [
                        'useMxCheck'        => false,
                        'useDeepMxCheck'    => false,
                        'useDomainCheck'    => true,
                        'allow'             => Hostname::ALLOW_DNS,
                        'strict'            => true,
                        'hostnameValidator' => null,
                    ]
                ]
            ]
        ];

        $filter['user_lang_iso'] = [
            'required'   => true,
            'filters'    => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => $this->supportedLanguages
                    ]
                ]
            ]
        ];

        return $filter;
    }

}
