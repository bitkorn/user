<?php

namespace Bitkorn\User\Service\Right;

use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\User\Table\User\Right\UserRightRelationTable;
use Bitkorn\User\Table\User\ViewUserRightRelationTable;

class UserRightRelationService extends AbstractService
{
    protected UserRightRelationTable $userRightRelationTable;
    protected ViewUserRightRelationTable $viewUserRightRelationTable;

    public function setUserRightRelationTable(UserRightRelationTable $userRightRelationTable): void
    {
        $this->userRightRelationTable = $userRightRelationTable;
    }

    public function setViewUserRightRelationTable(ViewUserRightRelationTable $viewUserRightRelationTable): void
    {
        $this->viewUserRightRelationTable = $viewUserRightRelationTable;
    }

    /**
     * @param string $userUuid
     * @param int $userRightId
     * @return string
     */
    public function insertUserRightRelation(string $userUuid, int $userRightId): string
    {
        return $this->userRightRelationTable->insertUserRightRelation($userUuid, $userRightId);
    }

    /**
     * @param string $userRightRelationUuid
     * @return bool
     */
    public function deleteUserRightRelation(string $userRightRelationUuid): bool
    {
        return $this->userRightRelationTable->deleteUserRightRelation($userRightRelationUuid) >= 0;
    }

    /**
     * @param string $userUuid
     * @param int $userRightId
     * @return bool
     */
    public function deleteUserRightRelationWithKeys(string $userUuid, int $userRightId): bool
    {
        return $this->userRightRelationTable->deleteUserRightRelationWithKeys($userUuid, $userRightId) >= 0;
    }
}
