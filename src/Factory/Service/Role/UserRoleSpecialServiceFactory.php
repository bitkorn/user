<?php

namespace Bitkorn\User\Factory\Service\Role;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\User\Entity\User\Role\UserRoleSpecialEntity;
use Bitkorn\User\Service\Role\UserRoleSpecialService;
use Bitkorn\User\Table\User\Role\UserRoleSpecialTable;
use Bitkorn\User\Table\User\Role\UserRoleSpecialRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserRoleSpecialServiceFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserRoleSpecialService();
        $service->setUserRoleSpecialEntity(new UserRoleSpecialEntity());
        $service->setUserRoleSpecialTable($container->get(UserRoleSpecialTable::class));
        $service->setUserRoleTable($container->get(UserRoleTable::class));
        $service->setUserRoleSpecialRelationTable($container->get(UserRoleSpecialRelationTable::class));
        return $service;
    }
}
