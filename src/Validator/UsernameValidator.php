<?php

namespace Bitkorn\User\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception;

class UsernameValidator extends AbstractValidator
{
    const WRONG_CHARACTERS = 'wrongCharacters';
    protected string $pattern = '/^[a-zA-Z][a-zA-Z0-9._#+-]{2,29}$/';
    protected array $matches = [];

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected array $messageTemplates = [
        self::WRONG_CHARACTERS     => "Nur folgende Zeichen a-z A-Z 0-9 ._#+- (min. 3 max. 30)",
    ];

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        $this->setValue($value);
        $res = preg_match_all($this->pattern, $value, $this->matches);
        if (empty($res)) {
            $this->error(self::WRONG_CHARACTERS);
            return false;
        }
//        $arr = str_split($this->value);
//        foreach ($arr as $char) {
//            $res = preg_match($this->pattern, $char, $this->matches);
//            if($res === false || $res < 1) {
//                $this->error(self::WRONG_CHARACTERS);
//                return false;
//            }
//        }
        return true;
    }
}
