<?php

namespace Bitkorn\User\Controller\Admin\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\User\Service\Group\UserGroupRelationService;
use Bitkorn\User\Service\Group\UserGroupService;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class UserGroupManagementController extends AbstractUserController
{
    protected UserGroupService $userGroupService;
    protected UserGroupRelationService $userGroupRelationService;

    public function setUserGroupService(UserGroupService $userGroupService): void
    {
        $this->userGroupService = $userGroupService;
    }

    public function setUserGroupRelationService(UserGroupRelationService $userGroupRelationService): void
    {
        $this->userGroupRelationService = $userGroupRelationService;
    }

    /**
     * @return JsonModel
     */
    public function deleteUserGroupRelationAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isDelete()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $userUuid = $this->params('user_uuid');
        $groupUuid = $this->params('group_uuid');
        $uuid = new Uuid();
        if (!$uuid->isValid($userUuid) || !$uuid->isValid($groupUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if ($this->userGroupRelationService->deleteUserGroupRelationWithUuids($userUuid, $groupUuid)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
