<?php

namespace Bitkorn\User\Service\Role;

use Bitkorn\User\Entity\User\Role\UserRoleSpecialRelationEntity;
use Bitkorn\User\Table\User\Role\UserRoleSpecialRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleSpecialTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;

class UserRoleSpecialRelationService
{
    protected UserRoleSpecialRelationTable $userRoleSpecialRelationTable;
    protected UserRoleTable $userRoleTable;
    protected UserRoleSpecialTable $userRoleSpecialTable;
    /**
     * @var UserRoleSpecialRelationEntity[]
     */
    protected array $userRoleSpecialRelationEntities;

    public function setUserRoleSpecialRelationTable(UserRoleSpecialRelationTable $userRoleSpecialRelationTable): void
    {
        $this->userRoleSpecialRelationTable = $userRoleSpecialRelationTable;
    }

    public function setUserRoleTable(UserRoleTable $userRoleTable): void
    {
        $this->userRoleTable = $userRoleTable;
    }

    public function setUserRoleSpecialTable(UserRoleSpecialTable $userRoleSpecialTable): void
    {
        $this->userRoleSpecialTable = $userRoleSpecialTable;
    }

    /**
     * @return UserRoleSpecialRelationEntity[]
     */
    public function getUserRoleSpecialRelationEntities(): array
    {
        return $this->userRoleSpecialRelationEntities;
    }

    /**
     * God only!
     *
     * @return bool
     */
    public function loadUserRoleSpecialRelationsAll(): bool
    {
        $all = $this->userRoleSpecialRelationTable->getUserRoleSpecialRelationsAllComplete();
        foreach ($all as $relation) {
            $userRoleSpecialRelationEntity = new UserRoleSpecialRelationEntity();
            if (!$userRoleSpecialRelationEntity->exchangeArrayFromDatabase($relation)) {
                return false;
            }
            $this->userRoleSpecialRelationEntities[] = $userRoleSpecialRelationEntity;
        }
        return true;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return array data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function getUserRoleSpecialRelationEntitiesData(): array
    {
        $data = [];
        foreach ($this->userRoleSpecialRelationEntities as $userRoleSpecialRelationEntit) {
            $data[] = $userRoleSpecialRelationEntit->getStorage();
        }
        return $data;
    }

    /**
     * @param string $userRoleSpecialRelationUuid
     * @param int $userRoleSpecialRelationValue
     * @return int -1 on error, 0 on success and nothing to update, 1 on success
     */
    public function updateUserRoleSpecialRelationValue(string $userRoleSpecialRelationUuid, int $userRoleSpecialRelationValue): int
    {
        return $this->userRoleSpecialRelationTable->updateUserRoleSpecialRelationValue($userRoleSpecialRelationUuid, $userRoleSpecialRelationValue);
    }

    public function createUserRoleSpecialRelations(): bool
    {
        $userRoles = $this->userRoleTable->getUserRoles();
        $userRoleSpecials = $this->userRoleSpecialTable->getUserRoleSpecials();
        foreach ($userRoles as $userRole) {
            foreach ($userRoleSpecials as $userRoleSpecial) {
                if (empty($this->userRoleSpecialRelationTable->insertUserRoleSpecialRelation($userRoleSpecial['user_role_special_uuid'], $userRole['user_role_id'], $userRoleSpecial['user_role_special_default_value']))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param string $userRoleSpecialRelationUuid
     * @return bool
     */
    public function existUserRoleSpecialRelation(string $userRoleSpecialRelationUuid): bool
    {
        return !empty($this->userRoleSpecialRelationTable->getUserRoleSpecialRelation($userRoleSpecialRelationUuid));
    }

}
