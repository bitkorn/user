<?php

namespace Bitkorn\User\View\Helper;

use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Bitkorn\User\Service\UserService;
use Laminas\Log\Logger;

class IsUserInRoleAlias extends AbstractViewHelper
{

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserService $userService
     */
    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    /**
     * @param string $userRoleAlias
     * @param bool $doMin
     * @return bool
     */
    public function __invoke(string $userRoleAlias, bool $doMin = true)
    {
        if($userRoleAlias == 'all') {
            return $this->userService->checkUserContainer();
        }
        if($doMin) {
            return $this->userService->checkUserRoleAliasAccessMin($userRoleAlias);
        } else {
            return $this->userService->checkUserRoleAliasAccess($userRoleAlias);
        }
    }
}
