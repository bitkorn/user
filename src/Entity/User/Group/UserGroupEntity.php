<?php

namespace Bitkorn\User\Entity\User\Group;

use Bitkorn\Trinket\Entity\AbstractEntity;

class UserGroupEntity extends AbstractEntity
{
    protected array $mapping = [
        'user_group_uuid'          => 'user_group_uuid',
        'user_group_alias'         => 'user_group_alias',
        'user_group_name'          => 'user_group_name',
        'user_group_desc'          => 'user_group_desc',
        'user_group_default'       => 'user_group_default',
        // user_group_relation
        'user_group_relation_uuid' => 'user_group_relation_uuid',
        'user_uuid'                => 'user_uuid',
    ];

    protected $primaryKey = 'user_group_uuid';

    public function getUserGroupAlias(): string
    {
        if (!isset($this->storage['user_group_alias'])) {
            return '';
        }
        return $this->storage['user_group_alias'];
    }

    public function getUserGroupName(): string
    {
        if (!isset($this->storage['user_group_name'])) {
            return '';
        }
        return $this->storage['user_group_name'];
    }

    public function getUserGroupDesc(): string
    {
        if (!isset($this->storage['user_group_desc'])) {
            return '';
        }
        return $this->storage['user_group_desc'];
    }

    public function getUserGroupDefault(): int
    {
        if (!isset($this->storage['user_group_default'])) {
            return 0;
        }
        return intval($this->storage['user_group_default']);
    }

}
