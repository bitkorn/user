<?php

namespace Bitkorn\User\Controller\Admin\Rest;

use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\User\Controller\AbstractUserRestController;
use Bitkorn\User\Form\User\UserForm;
use Bitkorn\User\Table\User\UserTable;
use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class UserRestController extends AbstractUserRestController
{
    protected UserTable $userTable;
    protected UserForm $userForm;

    public function setUserTable(UserTable $userTable): void
    {
        $this->userTable = $userTable;
    }

    public function setUserForm(UserForm $userForm): void
    {
        $this->userForm = $userForm;
    }

    /**
     * # POST /user-user
     *
     * Insert a new user and all the stuff that belongs to it.
     *
     * @param array $data **x-www-form-urlencoded**
     *     - **user_role_id**: 1=god, 2=admin, 3=manager, 4=staff, 5=custom, 10=customer
     *     - **user_login**: unique user login
     *     - **user_passwd**:
     *     - **user_passwd_confirm**:
     *     - **user_active**: 1=active, 0=not active
     * @return JsonModel
     *```
     *     {
     *         "desc": "",
     *         "auth": 1,
     *         "success": 1,
     *         "data": {
     *                     "user_uuid": "12345678-17e4-11e9-8e49-7824afbed67e"
     *                 },
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: If available, the new UUID from the generated record
     *     - **messages**: only in development mode. E.g. one or more messages from input validation
     * ### HTTP
     *     - 403 insufficient permissions (min user role 2)
     *     - 400 not all values are valid
     *     - 409 User login already exist
     *     - 201 success created
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     * @todo Brute-Force: HTTP 409 User login already exist
     *
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->userForm->setUserPasswdAvailable(true);
        $this->userForm->setUserRoleIdAvailable(true);
        $this->userForm->init();
        $this->userForm->setData($data);
        if (!$this->userForm->isValid()) {
            $jsonModel->addMessages($this->userForm->getMessages());
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->userService->existUserLogin($this->userForm->get('user_login')->getValue())) {
            $jsonModel->addMessage('User login already exist.');
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_409);
            return $jsonModel;
        }
        if ($this->userService->existUserEmail($this->userForm->get('user_email')->getValue())) {
            $jsonModel->addMessage('User email already exist.');
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_409);
            return $jsonModel;
        }

        if (empty($userUuid = $this->userService->createNewUser($this->userForm->getData()))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setVariable('user_uuid', $userUuid);
        $jsonModel->setSuccess(1);
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        return $jsonModel;
    }

    /**
     * # DELETE /user-user/:uuid
     *
     * Delete a user and everything that belongs to it
     *
     * @param string $id User UUID
     * @return JsonModel
     *```
     *     {
     *         "desc": "",
     *         "auth": 1,
     *         "success": 1,
     *         "data": [],
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: An empty JSON array.
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 403 insufficient permissions (min user role 1)
     *     - 200 success
     *     - 400 not all values are valid OR the alias isn't valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            $jsonModel->addMessage(AbstractUserController::MESSAGE_NOT_ENOUGH_RIGHTS);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->userService->deleteUser($id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * # GET /user-user
     *
     * Get details from the current user.
     *
     * @return JsonModel
     *```
     *     {
     *         "desc": "",
     *         "auth": 1,
     *         "success": 1,
     *         "data": [
     *                     {
     *                         "some_key": "12345678-17e4-11e9-8e49-7824afbed67e",
     *                         "another_key": "foo"
     *                     },
     *                     {
     *                         ...
     *                     }
     *                 ],
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: If available, the requested JSON array with data objects
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 403 insufficient permissions (min user role 4)
     *     - 200 success
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $jsonModel->addMessage(AbstractUserController::MESSAGE_NOT_ENOUGH_RIGHTS);
            return $jsonModel;
        }
        $jsonModel->setVariable('userDetails', $this->userService->getUserDetails($this->userService->getUserUuid()));
        return $jsonModel;
    }

    /**
     * # GET /user-user/:uuid
     *
     * Get one user with right's n roles.
     * Users can belong to several accounts. That's why the account of the current (logged in) user is taken here.
     * ...user UUID from url parameter and account uuid from the current (logged in) user.
     *
     * @param string $id A valid user UUID (URL Parameter)
     * @return JsonModel One JSON object
     *```
     *     {
     *         "desc": "",
     *         "auth": 1,
     *         "success": 1,
     *         "data": {
     *                     "some_key": "12345678-17e4-11e9-8e49-7824afbed67e",
     *                     "another_key": "foo"
     *                 },
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: If available, the requested JSON data object
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 403 insufficient permissions (min user role 5)
     *     - 400 not all values are valid
     *     - 200 success
     *     - 204 Data not found
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $jsonModel->addMessage(AbstractUserController::MESSAGE_NOT_ENOUGH_RIGHTS);
            return $jsonModel;
        }
        $jsonModel->setVariable('userWithRightsnRoles', $this->userService->getUserWithRightsnRoles($id));
        return $jsonModel;
    }

}
