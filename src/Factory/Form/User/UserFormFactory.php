<?php

namespace Bitkorn\User\Factory\Form\User;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Bitkorn\User\Form\User\UserForm;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserFormFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when creating a service.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new UserForm();
        /** @var UserRoleTable $userRoleTable */
        $userRoleTable = $container->get(UserRoleTable::class);
        $form->setUserRoleIdAssoc($userRoleTable->getUserRoleIdAssoc());
        $config = $container->get('config');
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setSupportedLanguages($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        return $form;
    }
}
