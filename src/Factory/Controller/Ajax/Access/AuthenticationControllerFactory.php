<?php

namespace Bitkorn\User\Factory\Controller\Ajax\Access;

use Bitkorn\User\Service\RightsnrolesRoutesService;
use Interop\Container\ContainerInterface;
use Bitkorn\User\Service\UserService;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Bitkorn\User\Controller\Ajax\Access\AuthenticationController;

class AuthenticationControllerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AuthenticationController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $config = $container->get('config');
        $controller->setSessionLifetime($config['bitkorn_user']['session_lifetime']);
        $controller->setCookiesEnabled($config['bitkorn_user']['cookies_enabled']);
        $controller->setRightsnrolesRoutesService($container->get(RightsnrolesRoutesService::class));
        return $controller;
    }
}
