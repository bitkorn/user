<?php


namespace Bitkorn\User\Form\User\Role;

use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class UserRoleSpecialForm extends Form implements InputFilterProviderInterface
{

    private $userRoleSpecialUuidAvailable = true;

    /**
     *
     */
    public function init()
    {
        if ($this->userRoleSpecialUuidAvailable) {
            $this->add(['name' => 'user_role_special_uuid']);
        }
        $this->add(['name' => 'user_role_special_alias']);
        $this->add(['name' => 'user_role_special_desc']);
    }

    /**
     * Should return an array specification compatible with
     * {@link Laminas\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->userRoleSpecialUuidAvailable) {
            $filter['user_role_special_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                    ['name' => StripTags::class],
                ], 'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }

        $filter['user_role_special_alias'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 60,
                    ]
                ]
            ]
        ];

        $filter['user_role_special_desc'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 200,
                    ]
                ]
            ]
        ];

        return $filter;
    }

    /**
     * @param bool $userRoleSpecialUuidAvailable
     */
    public function setUserRoleSpecialUuidAvailable(bool $userRoleSpecialUuidAvailable): void
    {
        $this->userRoleSpecialUuidAvailable = $userRoleSpecialUuidAvailable;
    }

}
