<?php

namespace Bitkorn\User\Factory\Service;


use Bitkorn\Trinket\Service\Session\RedisService;
use Exception;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\User\Service\UserService;
use Bitkorn\User\Table\User\Group\UserGroupRelationTable;
use Bitkorn\User\Table\User\Group\UserGroupTable;
use Bitkorn\User\Table\User\Right\UserRightRelationTable;
use Bitkorn\User\Table\User\Right\UserRightTable;
use Bitkorn\User\Table\User\Role\UserRoleRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleSpecialRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Bitkorn\User\Table\User\UserTable;
use Laminas\EventManager\EventManager;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserServiceFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when creating a service.
     * @throws Exception
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserService();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setConfigUser($config['bitkorn_user']);
        $service->setUserTable($container->get(UserTable::class));
        $service->setUserRoleTable($container->get(UserRoleTable::class));
        $service->setUserRoleSpecialRelationTable($container->get(UserRoleSpecialRelationTable::class));
        $service->setUserRoleRelationTable($container->get(UserRoleRelationTable::class));
        $service->setUserRightTable($container->get(UserRightTable::class));
        $service->setUserRightRelationTable($container->get(UserRightRelationTable::class));
        $service->setUserGroupTable($container->get(UserGroupTable::class));
        $service->setUserGroupRelationTable($container->get(UserGroupRelationTable::class));
        /** @var RedisService $rs */
        $rs = $container->get(RedisService::class);
        $service->setRedis($rs->getRedis());
        return $service;
    }
}
