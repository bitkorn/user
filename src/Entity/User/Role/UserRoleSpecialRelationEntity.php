<?php

namespace Bitkorn\User\Entity\User\Role;


use Bitkorn\Trinket\Entity\AbstractEntity;

class UserRoleSpecialRelationEntity extends AbstractEntity
{

    protected array $mapping = [
        'user_role_special_relation_uuid' => 'user_role_special_relation_uuid',
        'user_role_special_uuid' => 'user_role_special_uuid',
        'user_role_id' => 'user_role_id',
        'user_role_special_relation_value' => 'user_role_special_relation_value',
        // user_role_special
        'user_role_special_alias' => 'user_role_special_alias',
        'user_role_special_name' => 'user_role_special_name',
        'user_role_special_desc' => 'user_role_special_desc',
        'user_role_special_time_create' => 'user_role_special_time_create',
        'user_role_special_order_priority' => 'user_role_special_order_priority',
        // user_role
        'user_role_alias' => 'user_role_alias',
        'user_role_customizable' => 'user_role_customizable',
        'user_role_desc' => 'user_role_desc',
    ];

    /**
     * @return string
     */
    public function getUserRoleSpecialAlias(): string
    {
        if (!isset($this->storage['user_role_special_alias'])) {
            return '';
        }
        return $this->storage['user_role_special_alias'];
    }

    /**
     * @return int 0 or 1
     */
    public function getUserRoleSpecialRelationValue(): int
    {
        if (!isset($this->storage['user_role_special_relation_value'])) {
            return 0;
        }
        return (int) $this->storage['user_role_special_relation_value'];
    }

    public function userRoleSpecialRelationEnabled(): bool
    {
        return $this->getUserRoleSpecialRelationValue() > 0;
    }
}
