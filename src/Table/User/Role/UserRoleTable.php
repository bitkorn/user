<?php

namespace Bitkorn\User\Table\User\Role;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class UserRoleTable extends AbstractLibTable
{
    protected $table = 'user_role';

    /**
     * @return array
     */
    public function getUserRoles(): array
    {
        $select = $this->sql->select();
        try {
            $select->order('user_role_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $userUuid
     * @return array One role from user roles
     */
    public function getUserRoleForUser(string $userUuid): array
    {
        $select = $this->sql->select();
        try {
            $selectUserRoleRelation = new Select('user_role_relation');
            $selectUserRoleRelation->columns(['user_role_id']);
            $selectUserRoleRelation->where([
                'user_uuid' => $userUuid
            ]);

            $select->where->in('user_role_id', $selectUserRoleRelation);
            $select->order('user_role_id DESC');

            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if ($result->count() > 1) {
                    $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() - There is more than one role for user_uuid ' . $userUuid);
                }
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array The role-id-alias-assoc with all roles `ORDER BY user_role_id ASC`.
     */
    public function getUserRoleIdAssoc(): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->order('user_role_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArray = $result->toArray();
                foreach ($resultArray as $resultRow) {
                    $idAssoc[$resultRow['user_role_id']] = $resultRow['user_role_alias'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }
}
