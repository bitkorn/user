<?php

namespace Bitkorn\User\Service\Role;

use Bitkorn\User\Entity\User\Role\UserRoleSpecialEntity;
use Bitkorn\User\Table\User\Role\UserRoleSpecialTable;
use Bitkorn\User\Table\User\Role\UserRoleSpecialRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;

class UserRoleSpecialService
{
    private UserRoleSpecialEntity $userRoleSpecialEntity;
    private UserRoleSpecialTable $userRoleSpecialTable;
    private UserRoleTable $userRoleTable;
    private UserRoleSpecialRelationTable $userRoleSpecialRelationTable;

    public function setUserRoleSpecialEntity(UserRoleSpecialEntity $userRoleSpecialEntity): void
    {
        $this->userRoleSpecialEntity = $userRoleSpecialEntity;
    }

    public function setUserRoleSpecialTable(UserRoleSpecialTable $userRoleSpecialTable): void
    {
        $this->userRoleSpecialTable = $userRoleSpecialTable;
    }

    public function setUserRoleTable(UserRoleTable $userRoleTable): void
    {
        $this->userRoleTable = $userRoleTable;
    }

    public function setUserRoleSpecialRelationTable(UserRoleSpecialRelationTable $userRoleSpecialRelationTable): void
    {
        $this->userRoleSpecialRelationTable = $userRoleSpecialRelationTable;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function loadUserRoleSpecialEntity(array $data): bool
    {
        if (empty($data) || !is_array($data)) {
            return false;
        }
        if (!$this->userRoleSpecialEntity->exchangeArrayFromDatabase($data)) {
            return false;
        }
        if (empty($this->userRoleSpecialEntity->getUserRoleSpecialAlias())) {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function existUserRoleSpecialAlias(): bool
    {
        return $this->userRoleSpecialTable->existAlias($this->userRoleSpecialEntity->getUserRoleSpecialAlias());
    }

    /**
     * Insert new UserRoleExtra with his UserRoleExtraValues as SQL transaction (roll back on error).
     *
     * @return string
     */
    public function insertNewUserRoleSpecial(): string
    {
        /** @var Adapter $adapter */
        $adapter = $this->userRoleSpecialTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $success = true;
        $userRoleSpecialUuid = $this->userRoleSpecialTable->insertUserRoleSpecial($this->userRoleSpecialEntity->getUserRoleSpecialAlias(), $this->userRoleSpecialEntity->getUserRoleSpecialDesc());
        if (empty($userRoleSpecialUuid)) {
            return '';
        }
        $userRoleIdAssoc = $this->userRoleTable->getUserRoleIdAssoc();
        foreach ($userRoleIdAssoc as $roleId => $roleAlias) {
            if (empty($this->userRoleSpecialRelationTable->insertUserRoleSpecialRelation($userRoleSpecialUuid, $roleId, 0))) {
                $success = false;
            }
        }
        if (!$success) {
            $connection->rollback();
            return '';
        }

        $connection->commit();
        return $userRoleSpecialUuid;
    }

    /**
     * @param string $roleSpecialUuid
     * @return int -1 on error; 0 if success but nothing deleted; 1 if success deleted
     */
    public function deleteUserRoleSpecial(string $roleSpecialUuid): int
    {
        /** @var Adapter $adapter */
        $adapter = $this->userRoleSpecialRelationTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if (!$this->userRoleSpecialRelationTable->deleteUserRoleSpecialRelationByRoleSpecialUuid($roleSpecialUuid)) {
            $connection->rollback();
            return -1;
        }

        $result = $this->userRoleSpecialTable->deleteUserRoleSpecial($roleSpecialUuid);
        if ($result < 0) {
            $connection->rollback();
            return -1;
        }
        $connection->commit();
        return $result;
    }

}
