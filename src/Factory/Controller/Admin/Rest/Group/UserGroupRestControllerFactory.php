<?php

namespace Bitkorn\User\Factory\Controller\Admin\Rest\Group;

use Bitkorn\User\Controller\Admin\Rest\Group\UserGroupRestController;
use Bitkorn\User\Form\User\Group\UserGroupForm;
use Bitkorn\User\Service\Group\UserGroupService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserGroupRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new UserGroupRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setUserGroupService($container->get(UserGroupService::class));
        $controller->setUserGroupForm($container->get(UserGroupForm::class));
        return $controller;
    }
}
