<?php

namespace Bitkorn\User\Service\Right;

use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\User\Table\User\Right\UserRightRelationTable;
use Bitkorn\User\Table\User\Right\UserRightTable;
use Bitkorn\User\Table\User\ViewUserRightRelationTable;

class UserRightService extends AbstractService
{
    protected UserRightTable $userRightTable;
    protected UserRightRelationTable $userRightRelationTable;
    protected ViewUserRightRelationTable $viewUserRightRelationTable;

    public function setUserRightTable(UserRightTable $userRightTable): void
    {
        $this->userRightTable = $userRightTable;
    }

    public function setUserRightRelationTable(UserRightRelationTable $userRightRelationTable): void
    {
        $this->userRightRelationTable = $userRightRelationTable;
    }

    public function setViewUserRightRelationTable(ViewUserRightRelationTable $viewUserRightRelationTable): void
    {
        $this->viewUserRightRelationTable = $viewUserRightRelationTable;
    }

    /**
     * @param string $userRightAlias
     * @return array From table db.view_user_right_relation.
     */
    public function getUsersWithRight(string $userRightAlias): array
    {
        return $this->viewUserRightRelationTable->getUsersWithRight($userRightAlias);
    }

    /**
     * @return array From view_user_right
     */
    public function getUserRights(): array
    {
        return $this->userRightTable->getUserRightsWithUserUuids();
    }
}
