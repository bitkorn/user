<?php

namespace Bitkorn\User\Table\User\Group;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class UserGroupTable extends AbstractLibTable
{
    /**
     * @var string
     */
    protected $table = 'user_group';

    /**
     * @param string $userGroupName
     * @param string $userGroupAlias
     * @param string $userGroupDesc
     * @param int $userGroupDefault
     * @return string New created user_group_uuid
     */
    public function insertUserGroup(string $userGroupName, string $userGroupAlias, string $userGroupDesc, int $userGroupDefault = 0): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'user_group_uuid'    => $uuid,
                'user_group_name'    => $userGroupName,
                'user_group_alias'   => $userGroupAlias,
                'user_group_desc'    => $userGroupDesc,
                'user_group_default' => $userGroupDefault
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $userGroupUuid
     * @return array
     */
    public function getUserGroup(string $userGroupUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_group_uuid' => $userGroupUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $resultArr = $result->toArray();
                return $resultArr[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array From view_user_group (additional field: user_uuids_csv)
     */
    public function getUserGroups(): array
    {
        $select = new Select('view_user_group');
        try {
            $select->order('user_group_name ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $userGroupUuid
     * @return int
     */
    public function deleteUserGroup(string $userGroupUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_group_uuid' => $userGroupUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @return string
     */
    public function getUserGroupUuidDefault(): string
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_group_default' => 1]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $current = $result->current();
                return $current['user_group_uuid'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @return int
     */
    public function unsetUserGroupDefault(): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_group_default' => 0]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userGroupUuid
     * @param string $userGroupName
     * @param string $userGroupDesc
     * @param int $userGroupDefault
     * @return int
     */
    public function updateUserGroup(string $userGroupUuid, string $userGroupName, string $userGroupDesc, int $userGroupDefault = 0): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'user_group_name'    => $userGroupName,
                'user_group_desc'    => $userGroupDesc,
                'user_group_default' => $userGroupDefault
            ]);
            $update->where(['user_group_uuid' => $userGroupUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }

        return -1;
    }

    /**
     * @return array With additional field 'user_uuids_csv'.
     */
    public function getUserGroupsWithUserUuids(): array
    {
        $select = new Select('view_user_group');
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
