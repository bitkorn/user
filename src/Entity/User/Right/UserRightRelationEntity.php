<?php

namespace Bitkorn\User\Entity\User\Right;


use Bitkorn\Trinket\Entity\AbstractEntity;

class UserRightRelationEntity extends AbstractEntity
{

    protected array $mapping = [
        '$user_right_relation_uuid' => '$user_right_relation_uuid',
        'user_uuid' => 'user_uuid',
        'user_right_relation_value' => 'user_right_relation_value',
        // JOIN user_right
        'user_right_id' => 'user_right_id',
        'user_right_alias' => 'user_right_alias',
        'user_right_desc' => 'user_right_desc',
    ];

    /**
     * @return int
     */
    public function getUserRightRelationValue(): int
    {
        if (!isset($this->storage['user_right_relation_value'])) {
            return 0;
        }
        return (int) $this->storage['user_right_relation_value'];
    }

    /**
     * @return bool
     */
    public function userRightRelationEnabled(): bool
    {
        return $this->storage['user_right_relation_value'] > 0;
    }

    /**
     * @return int
     */
    public function getUserRightId(): int
    {
        if (!isset($this->storage['user_right_id'])) {
            return 0;
        }
        return (int) $this->storage['user_right_id'];
    }

    /**
     * @return string
     */
    public function getUserRightAlias(): string
    {
        if (!isset($this->storage['user_right_alias'])) {
            return '';
        }
        return $this->storage['user_right_alias'];
    }

    /**
     * @return string
     */
    public function getUserRightDesc(): string
    {
        if (!isset($this->storage['user_right_desc'])) {
            return '';
        }
        return $this->storage['user_right_desc'];
    }

}
