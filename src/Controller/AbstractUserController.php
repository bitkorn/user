<?php

namespace Bitkorn\User\Controller;

use Bitkorn\User\Service\UserService;
use Bitkorn\Trinket\Controller\AbstractHtmlController;
use Laminas\Mvc\MvcEvent;

class AbstractUserController extends AbstractHtmlController
{
    const MESSAGE_ONLY_GOD = 'Only God is allowed to be creative.';
    const MESSAGE_NOT_ENOUGH_RIGHTS = 'Not enough rights to perform this action.';
    const MESSAGE_ERROR_WHILE_INSERT_DATA = 'Error while insert the data.';
    const MESSAGE_ERROR_WHILE_UPDATE_DATA = 'Error while update the data.';
    const MESSAGE_ERROR_WHILE_SELECT_DATA = 'Error while load the data.';
    const MESSAGE_CAN_NOT_LOAD_ENTITY = 'Can not load entity.';
    const MESSAGE_SCRIPT_KIDDIE = 'You are script kiddie.';

    protected UserService $userService;

    /**
     * @param UserService $userService
     */
    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    public function onDispatch(MvcEvent $e)
    {
        if ($this->userService->checkUserContainer()) {
            $userEntity = $this->userService->getUserEntity();
            $e->getViewModel()->setVariable('userLogin', $userEntity->getUserLogin());
            $e->getViewModel()->setVariable('loggedin', true);
            $e->getViewModel()->setVariable('isGod', $this->userService->checkUserRoleAccess(1));
        }
        return parent::onDispatch($e);
    }
}
