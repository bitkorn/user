<?php

namespace Bitkorn\User\Factory\Service\Right;

use Bitkorn\User\Service\Right\UserRightRelationService;
use Bitkorn\User\Table\User\Right\UserRightRelationTable;
use Bitkorn\User\Table\User\ViewUserRightRelationTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserRightRelationServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserRightRelationService();
        $service->setLogger($container->get('logger'));
        $service->setUserRightRelationTable($container->get(UserRightRelationTable::class));
        $service->setViewUserRightRelationTable($container->get(ViewUserRightRelationTable::class));
        return $service;
    }
}
