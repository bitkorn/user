<?php

namespace Bitkorn\User\Service\Group;

use Bitkorn\Trinket\Tools\String\StringTool;
use Bitkorn\User\Entity\User\Group\UserGroupEntity;
use Bitkorn\User\Table\User\Group\UserGroupRelationTable;
use Bitkorn\User\Table\User\Group\UserGroupTable;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;

class UserGroupService extends AbstractService
{
    protected UserGroupTable $userGroupTable;
    protected UserGroupRelationTable $userGroupRelationTable;

    public function setUserGroupTable(UserGroupTable $userGroupTable): void
    {
        $this->userGroupTable = $userGroupTable;
    }

    public function setUserGroupRelationTable(UserGroupRelationTable $userGroupRelationTable): void
    {
        $this->userGroupRelationTable = $userGroupRelationTable;
    }

    /**
     * @return array From view_user_group (additional field: user_uuids_csv)
     */
    public function getUserGroups(): array
    {
        return $this->userGroupTable->getUserGroups();
    }

    /**
     * @param string $userGroupUuid
     * @return bool
     */
    public function existUserGroup(string $userGroupUuid): bool
    {
        return !empty($this->userGroupTable->getUserGroup($userGroupUuid));
    }

    /**
     * @param array $data
     * @return string UUID from inserted userGroup
     */
    public function insertUserGroup(array $data): string
    {
        $userGroupEntity = new UserGroupEntity();
        if (!$userGroupEntity->exchangeArrayFromDatabase($data)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while exchange array for UserGroupEntity.');
            return '';
        }
        /** @var Adapter $adapter */
        $adapter = $this->userGroupTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if ($userGroupEntity->getUserGroupDefault() == 1) {
            if ($this->userGroupTable->unsetUserGroupDefault() < 0) {
                $connection->rollback();
                return '';
            }
        }
        if(empty($name = $userGroupEntity->getUserGroupName())) {
            $connection->rollback();
            return '';
        }

        $userGroupUuid = $this->userGroupTable->insertUserGroup($name, StringTool::computeAlias($name), $userGroupEntity->getUserGroupDesc(), $userGroupEntity->getUserGroupDefault());
        if (empty($userGroupUuid)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while insert userGroup');
            $connection->rollback();
            return '';
        }

        $connection->commit();
        return $userGroupUuid;
    }

    /**
     * @param string $userGroupUuid
     * @return bool
     */
    public function deleteUserGroupComplete(string $userGroupUuid): bool
    {
        /** @var Adapter $adapter */
        $adapter = $this->userGroupTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if ($this->userGroupRelationTable->deleteForUserGroup($userGroupUuid) < 0) {
            $connection->rollback();
            return false;
        }

        if (!$this->userGroupTable->deleteUserGroup($userGroupUuid) < 0) {
            $connection->rollback();
            return false;
        }

        $connection->commit();
        return true;
    }

    /**
     *
     * @param array $userGroupData
     * @return int -1 on error, 0 on success but nothing to update, 1 on success update
     */
    public function updateUserGroup(array $userGroupData): int
    {
        $userGroupEntity = new UserGroupEntity();
        if (!$userGroupEntity->exchangeArrayFromDatabase($userGroupData)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while exchange array.');
            return '';
        }
        /** @var Adapter $adapter */
        $adapter = $this->userGroupTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if ($userGroupEntity->getUserGroupDefault() == 1) {
            if ($this->userGroupTable->unsetUserGroupDefault() < 0) {
                $connection->rollback();
                return -1;
            }
        }

        $result = $this->userGroupTable->updateUserGroup($userGroupEntity->getUuid(), $userGroupEntity->getUserGroupName(), $userGroupEntity->getUserGroupDesc(), $userGroupEntity->getUserGroupDefault());
        if ($result < 0) {
            $connection->rollback();
            return -1;
        }
        $connection->commit();
        return $result;
    }
}
