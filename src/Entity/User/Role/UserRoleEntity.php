<?php


namespace Bitkorn\User\Entity\User\Role;


use Bitkorn\Trinket\Entity\AbstractEntity;

class UserRoleEntity extends AbstractEntity
{
    protected array $mapping = [
        'user_role_id' => 'user_role_id',
        'user_role_alias' => 'user_role_alias',
        'user_role_customizable' => 'user_role_customizable',
        'user_role_desc' => 'user_role_desc',
    ];

    /**
     * @return int
     */
    public function getUserRoleId(): int
    {
        if (!isset($this->storage['user_role_id'])) {
            return -1;
        }
        return intval($this->storage['user_role_id']);
    }

    /**
     * @return string
     */
    public function getUserRoleAlias(): string
    {
        if (!isset($this->storage['user_role_alias'])) {
            return '';
        }
        return $this->storage['user_role_alias'];
    }
}
