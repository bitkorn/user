<?php

namespace Bitkorn\User\Factory\Controller\Admin\Ajax;

use Bitkorn\User\Controller\Admin\Ajax\UserGroupManagementController;
use Bitkorn\User\Service\Group\UserGroupRelationService;
use Bitkorn\User\Service\Group\UserGroupService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserGroupManagementControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new UserGroupManagementController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setUserGroupService($container->get(UserGroupService::class));
        $controller->setUserGroupRelationService($container->get(UserGroupRelationService::class));
        return $controller;
    }
}
