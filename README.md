User management module for Laminas.

# Events
- UserService: `$this->events->trigger('before_login', $this, ['login' => $login, 'passwd' => $password])`
- UserService: `$this->events->trigger('after_create_user', $this, $storage)`
- UserService: `$this->events->trigger('before_delete_user', $this, ['user_uuid' => $userUuid]);`
