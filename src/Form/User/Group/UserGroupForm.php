<?php

namespace Bitkorn\User\Form\User\Group;

use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Filter\ToInt;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class UserGroupForm extends Form implements InputFilterProviderInterface
{
    protected bool $userGroupUuidAvailable = true;

    public function setUserGroupUuidAvailable(bool $userGroupUuidAvailable): void
    {
        $this->userGroupUuidAvailable = $userGroupUuidAvailable;
    }

    /**
     *
     */
    public function init()
    {
        if ($this->userGroupUuidAvailable) {
            $this->add(['name' => 'user_group_uuid']);
        }
        $this->add(['name' => 'user_group_name']);
        $this->add(['name' => 'user_group_desc']);
        $this->add(['name' => 'user_group_default']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->userGroupUuidAvailable) {
            $filter['user_group_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => StringTrim::class],
                    ['name' => StripTags::class],
                ], 'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }

        $filter['user_group_name'] = [
            'required'      => true,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 3,
                        'max'      => 60,
                    ]
                ]
            ]
        ];

        $filter['user_group_desc'] = [
            'required'      => false,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 180,
                    ]
                ]
            ]
        ];

        $filter['user_group_default'] = [
            'required' => true,
            'filters'  => [
                ['name' => ToInt::class]
            ]
        ];

        return $filter;
    }

}
