<?php


namespace Bitkorn\User\Factory\Controller\Admin\Rest\Role;


use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\User\Controller\Admin\Rest\Role\UserRoleSpecialController;
use Bitkorn\User\Form\User\Role\UserRoleSpecialForm;
use Bitkorn\User\Service\UserService;
use Bitkorn\User\Service\Role\UserRoleSpecialService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserRoleSpecialControllerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new UserRoleSpecialController();
        $controller->setUserService($container->get(UserService::class));
        $controller->setUserRoleSpecialForm($container->get(UserRoleSpecialForm::class));
        $controller->setUserRoleSpecialService($container->get(UserRoleSpecialService::class));
        return $controller;
    }
}
