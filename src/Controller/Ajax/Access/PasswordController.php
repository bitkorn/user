<?php

namespace Bitkorn\User\Controller\Ajax\Access;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;

class PasswordController extends AbstractUserController
{
    /**
     * @return JsonModel
     */
    public function updatePasswdAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (
            !$request->isPost()
            || empty($passwdOld = $request->getPost('passwd0'))
            || empty($passwd1 = $request->getPost('passwd1'))
            || empty($passwd2 = $request->getPost('passwd2'))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->userService->checkUserPassword($this->userService->getUserUuid(), $passwdOld)) {
            $jsonModel->addMessage('Das aktuelle Passwort ist falsch.');
            return $jsonModel;
        }
        if ($passwd1 != $passwd2) {
            $jsonModel->addMessage('Die Passwörter sind nicht identisch.');
            return $jsonModel;
        }
        if ($this->userService->updateUserPasswd($this->userService->getUserUuid(), $passwd1)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
