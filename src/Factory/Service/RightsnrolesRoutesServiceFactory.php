<?php

namespace Bitkorn\User\Factory\Service;

use Bitkorn\User\Service\RightsnrolesRoutesService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RightsnrolesRoutesServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new RightsnrolesRoutesService();
        $service->setLogger($container->get('logger'));
        $service->setRoutes($container->get('config')['router']['routes']);
        return $service;
    }
}
