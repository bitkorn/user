<?php

namespace Bitkorn\User\Controller\Admin\Rest\Role;

use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\User\Controller\AbstractUserRestController;
use Bitkorn\User\Service\Role\UserRoleRelationService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class UserRoleRelationRestController extends AbstractUserRestController
{

    protected UserRoleRelationService $userRoleRelationService;

    public function setUserRoleRelationService(UserRoleRelationService $userRoleRelationService): void
    {
        $this->userRoleRelationService = $userRoleRelationService;
    }

    /**
     * # POST /user-admin-userroleRelation
     *
     * Insert or Update UserRoleRelation.
     *
     * @param array $data **x-www-form-urlencoded**
     *     - **user_uuid**: valid UUID
     *     - **user_role_id**: 1=god; 2=admin; 3=manager; 4=staff; 5=custom; 10=customer
     * @return JsonModel
     *```
     *     {
     *         "success": 0,
     *         "data": {
     *                     "user_role_relation_uuid": "12345678-17e4-11e9-8e49-7824afbed67e"
     *                 },
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: If available, the new UUID from the generated record
     *     - **messages**: only in development mode. E.g. one or more messages from input validation
     * ### HTTP
     *     - 403 insufficient permissions (user role 1)
     *     - 201 success created
     *     - 400 not all values are valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccess(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            $jsonModel->addMessage(AbstractUserController::MESSAGE_ONLY_GOD);
            return $jsonModel;
        }

        if (!(new Uuid())->isValid($data['user_uuid']) || empty($data['user_role_id'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        if (empty($userRoleRelationUuid = $this->userRoleRelationService->insertOrUpdateUserRoleRelation($data['user_uuid'], intval($data['user_role_id'])))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_409);
            $jsonModel->addMessage($this->userRoleRelationService->getMessage());
            return $jsonModel;
        }
        $jsonModel->setVariable('user_role_relation_uuid', $userRoleRelationUuid);
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);

        return $jsonModel;
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id) || empty($data['user_role_id'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!empty($this->userRoleRelationService->insertOrUpdateUserRoleRelation($id, intval($data['user_role_id'])))) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

}
