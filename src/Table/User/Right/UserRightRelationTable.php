<?php

namespace Bitkorn\User\Table\User\Right;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class UserRightRelationTable extends AbstractLibTable
{
    protected $table = 'user_right_relation';

    /**
     * @param string $userUuid
     * @param int $userRightId
     * @param int $userRightRelationValue
     * @return string
     */
    public function insertUserRightRelation(string $userUuid, int $userRightId, int $userRightRelationValue = 1): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'user_right_relation_uuid'  => $uuid,
                'user_uuid'                 => $userUuid,
                'user_right_id'             => $userRightId,
                'user_right_relation_value' => $userRightRelationValue,
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUserRightRelationsByUserId(string $userUuid): array
    {
        $select = $this->sql->select();
        try {

            $select->join('user_right', 'user_right.user_right_id = user_right_relation.user_right_id', Select::SQL_STAR, Select::JOIN_LEFT);

            $select->where(['user_right_relation.user_uuid' => $userUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function deleteForUser(string $userUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_uuid' => $userUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteUserRightRelation(string $userRightRelationUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_right_relation_uuid' => $userRightRelationUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteUserRightRelationWithKeys(string $userUuid, int $userRightId): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_uuid' => $userUuid, 'user_right_id' => $userRightId]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
