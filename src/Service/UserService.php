<?php

namespace Bitkorn\User\Service;

use Bitkorn\User\Entity\User\Rightsnroles;
use Bitkorn\User\Entity\User\UserEntity;
use Bitkorn\User\Entity\User\UserFormEntity;
use Bitkorn\User\Table\User\Group\UserGroupRelationTable;
use Bitkorn\User\Table\User\Group\UserGroupTable;
use Bitkorn\User\Table\User\Right\UserRightRelationTable;
use Bitkorn\User\Table\User\Right\UserRightTable;
use Bitkorn\User\Table\User\Role\UserRoleRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleSpecialRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Bitkorn\User\Table\User\UserTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\EventManager\EventManager;
use Laminas\EventManager\ResponseCollection;
use Laminas\Log\Logger;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;

/**
 * Uses redis hashes as session storage.
 * https://github.com/phpredis/phpredis#hashes
 *
 * @package Bitkorn\User\Service
 */
class UserService
{
    const REFERRER_KEY = 'LAST_REFERRER';
    const SESSION_KEY = 'SESSIONHASH';
    const SESSION_USERENTITY_KEY = 'USER_ENTITY';
    const SESSION_TIME_TOUCH_KEY = 'TIME_TOUCH';

    const EVENT_AFTER_CREATE_USER = 'after_create_user';
    const EVENT_BEFORE_DELETE_USER = 'before_delete_user';
    const EVENT_BEFORE_LOGIN = 'before_login';

    protected array $configUser;
    protected int $sessionLifetime;
    protected bool $cookiesEnabled;
    protected bool $registrationEnabled;
    private string $userSessionHash;
    protected Logger $logger;
    protected \Redis $redis;
    protected UserEntity $userEntity;
    protected Rightsnroles $rightsnroles;
    protected UserTable $userTable;
    protected UserRoleTable $userRoleTable;
    protected UserRoleSpecialRelationTable $userRoleSpecialRelationTable;
    protected UserRoleRelationTable $userRoleRelationTable;
    protected UserRightTable $userRightTable;
    protected UserRightRelationTable $userRightRelationTable;
    protected UserGroupTable $userGroupTable;
    protected UserGroupRelationTable $userGroupRelationTable;
    protected EventManager $events;
    protected bool $isSessionHashManually = false;

    /**
     * @var array The role-id-alias-assoc with all roles `ORDER BY user_role_id ASC`.
     */
    protected array $userRoleIdAssoc = [];

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        $this->events = new EventManager();
        $this->events->setIdentifiers([__CLASS__, get_class($this)]);
    }

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setRedis(\Redis $redis): void
    {
        $this->redis = $redis;
    }

    public function setRightsnroles(Rightsnroles $rightsnroles): void
    {
        $this->rightsnroles = $rightsnroles;
    }

    public function setConfigUser(array $configUser): void
    {
        $this->configUser = $configUser;
        $this->sessionLifetime = $this->configUser['session_lifetime'];
        $this->cookiesEnabled = $this->configUser['cookies_enabled'];
        $this->registrationEnabled = $this->configUser['registration_enabled'];
    }

    public function getSessionLifetime(): int
    {
        return $this->sessionLifetime;
    }

    public function setUserEntity(UserEntity $userEntity): void
    {
        $this->userEntity = $userEntity;
    }

    public function setUserTable(UserTable $userTable): void
    {
        $this->userTable = $userTable;
    }

    public function setUserRoleTable(UserRoleTable $userRoleTable): void
    {
        $this->userRoleTable = $userRoleTable;
        $this->userRoleIdAssoc = $this->userRoleTable->getUserRoleIdAssoc();
    }

    public function setUserRoleSpecialRelationTable(UserRoleSpecialRelationTable $userRoleSpecialRelationTable): void
    {
        $this->userRoleSpecialRelationTable = $userRoleSpecialRelationTable;
    }

    public function setUserRoleRelationTable(UserRoleRelationTable $userRoleRelationTable): void
    {
        $this->userRoleRelationTable = $userRoleRelationTable;
    }

    public function setUserRightTable(UserRightTable $userRightTable): void
    {
        $this->userRightTable = $userRightTable;
    }

    public function setUserRightRelationTable(UserRightRelationTable $userRightRelationTable): void
    {
        $this->userRightRelationTable = $userRightRelationTable;
    }

    public function setUserGroupTable(UserGroupTable $userGroupTable): void
    {
        $this->userGroupTable = $userGroupTable;
    }

    public function setUserGroupRelationTable(UserGroupRelationTable $userGroupRelationTable): void
    {
        $this->userGroupRelationTable = $userGroupRelationTable;
    }

    public function isRegistrationEnabled(): bool
    {
        return $this->registrationEnabled;
    }

    public function getEvents(): EventManager
    {
        return $this->events;
    }

    public function getRedis(): \Redis
    {
        return $this->redis;
    }

    public function canLogin(string $login, string $password): bool
    {
        return $this->userTable->canLogin($login, $this->computePasswordHash($password));
    }

    /**
     * Login Step 1
     * It refreshes the session hash. So, you must provide this new hash to the client.
     * @param string $login
     * @param string $password
     * @param bool $onlyAktive
     * @return bool
     * @todo $triggerResult->valid() or what?
     */
    public function loadWithLoginAndPassword(string $login, string $password, bool $onlyAktive = true): bool
    {
        /** @var ResponseCollection $triggerResult */
        $triggerResult = $this->events->trigger(self::EVENT_BEFORE_LOGIN, $this, ['login' => $login, 'passwd' => $password]);

        $userData = $this->userTable->getUserByLogin($login);
        if (empty($userData) || ($onlyAktive && $userData['user_active'] != 1) || $userData['user_passwd'] != $this->computePasswordHash($password)) {
            return false;
        }
        if (!$this->loadUserEntity($userData)) {
            return false;
        }
        $this->userSessionHash = $this->computeUserSessionHash();
        return $this->refreshSessionUserEntity();
    }

    /**
     * Fetch actual data for current logged-in user and refresh session data with it.
     * You must provide the actualized data to the client.
     * @return bool
     */
    public function loadWithSession(): bool
    {
        if (!$this->checkUserContainer()) {
            return false;
        }
        if (!$this->loadUserEntity($this->userTable->getUserByLogin($this->getUserEntity()->getUserLogin()))) {
            return false;
        }
        return $this->refreshSessionUserEntity();
    }

    protected function loadUserEntity(array $userData): bool
    {
        $this->userEntity = new UserEntity();
        if (!$this->userEntity->loadWithUserData($userData)) {
            return false;
        }
        if (!$this->userEntity->loadUserRole($this->userRoleTable)) {
            $this->logger->debug(print_r($this->userEntity, true));
            return false;
        }
        if (!$this->userEntity->loadUserRoleSpecialRelations($this->userRoleSpecialRelationTable)) {
            $this->logger->debug(__LINE__);
            return false;
        }
        if (!$this->userEntity->loadUserRightRelations($this->userRightRelationTable)) {
            $this->logger->debug(__LINE__);
            return false;
        }
        if (!$this->userEntity->loadUserGroupRelations($this->userGroupRelationTable)) {
            $this->logger->debug(__LINE__);
            return false;
        }
        return true;
    }

    /**
     * Insert a new user and all the stuff that belongs to it.
     * @param array $userData
     * @return string UUID from the new created user.
     */
    public function createNewUser(array $userData): string
    {
        $userEntity = new UserEntity();
        if (!$userEntity->exchangeArrayFromRequest($userData)) {
            return '';
        }
        /** @var Adapter $adapter */
        $adapter = $this->userTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $storage = $userEntity->getStorage();
        if (empty($userUuid = $this->userTable->insertUser($storage['user_login'], $this->computePasswordHash($storage['user_passwd']), $storage['user_active'], $storage['user_email'], $storage['user_lang_iso']))) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while insert User.');
            $connection->rollback();
            return '';
        }
        if (empty($this->userRoleRelationTable->insertUserRoleRelation($userUuid, $storage['user_role_id']))) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while insert UserRoleRelation.');
            $connection->rollback();
            return '';
        }
        $userGroupUuidDefault = $this->userGroupTable->getUserGroupUuidDefault();
        if (!empty($userGroupUuidDefault)) {
            if (empty($this->userGroupRelationTable->insertUserGroupRelation($userUuid, $userGroupUuidDefault))) {
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Error while insert UserGroupRelation.');
                $connection->rollback();
                return '';
            }
        }
        $connection->commit();

        $triggerResult = $this->events->trigger(self::EVENT_AFTER_CREATE_USER, $this, array_merge(['user_uuid' => $userUuid], $storage));
        return $userUuid;
    }

    /**
     * @param string $userUuid
     * @return bool
     */
    public function isValidUserUuid(string $userUuid): bool
    {
        return !empty($userUuid) && (new Uuid())->isValid($userUuid) && $this->existUser($userUuid);
    }

    /**
     * @param string $userUuid
     * @return bool
     */
    public function existUser(string $userUuid): bool
    {
        return $this->userTable->existUser($userUuid);
    }

    /**
     * @param string $userLogin
     * @return bool
     */
    public function existUserLogin(string $userLogin): bool
    {
        return $this->userTable->existUserLogin($userLogin);
    }

    /**
     * @param string $userEmail
     * @return bool
     */
    public function existUserEmail(string $userEmail): bool
    {
        return $this->userTable->existUserEmail($userEmail);
    }

    /**
     * Delete a user and everything that belongs to it.
     *
     * @param string $userUuid
     * @return bool
     */
    public function deleteUser(string $userUuid): bool
    {
        /** @var Adapter $adapter */
        $adapter = $this->userTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        $triggerResult = $this->events->trigger(self::EVENT_BEFORE_DELETE_USER, $this, ['user_uuid' => $userUuid]);
        if ($this->userGroupRelationTable->deleteForUser($userUuid) < 0) {
            $connection->rollback();
            return false;
        }
        if ($this->userRightRelationTable->deleteForUser($userUuid) < 0) {
            $connection->rollback();
            return false;
        }
        if ($this->userRoleRelationTable->deleteForUser($userUuid) < 0) {
            $connection->rollback();
            return false;
        }
        if ($this->userTable->deleteUser($userUuid) < 0) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    /**
     * @return array
     */
    public function getUserGroupUuids(): array
    {
        $userGroups = $this->userGroupTable->getUserGroups();
        if (empty($userGroups)) {
            return [];
        }
        $userGroupUuids = [];
        foreach ($userGroups as $userGroup) {
            $userGroupUuids[] = $userGroup['user_group_uuid'];
        }
        return $userGroupUuids;
    }

    /**
     * @return UserEntity
     */
    public function getUserEntity(): UserEntity
    {
        return $this->userEntity;
    }

    /**
     * @return string|null
     */
    public function getUserUuid(): ?string
    {
        if (empty($this->userEntity)) {
            return null;
        }
        return $this->userEntity->getUuid();
    }

    /**
     * @return array
     */
    public function getUserRoleIdsAssoc(): array
    {
        return $this->userRoleTable->getUserRoleIdAssoc();
    }

    /**
     * @param string $userUuid
     * @return string
     */
    public function getUserRoleRoute(string $userUuid): string
    {
        return $this->userRoleTable->getUserRoleForUser($userUuid)['user_role_route'] ?? 'bitkorn_user_html_dashboard_dashboard';
    }

    /**
     * @param string $password
     * @return string
     */
    private function computePasswordHash(string $password): string
    {
        return hash('sha512', $password);
    }

    /**
     * Creates a hash that does not exist yet.
     *
     * @return string
     */
    private function computeUserSessionHash(): string
    {
        $hash = hash('sha256', UserService::class . time() . rand(100000, 999999));
        try {
            if ($this->redis->hExists(UserService::SESSION_USERENTITY_KEY, $hash)) {
                return $this->computeUserSessionHash();
            }
        } catch (\RedisException $e) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() ' . $e->getMessage());
            return false;
        }
        return $hash;
    }

    /**
     * @return string
     */
    public function getUserSessionHash(): string
    {
        return $this->userSessionHash;
    }

    /**
     * Set user session (redis) container UserEntity.
     */
    protected function refreshSessionUserEntity(): bool
    {
        $userEntitySerialized = serialize($this->userEntity);
        try {
            if (!$this->redis->hSet($this->userSessionHash, UserService::SESSION_USERENTITY_KEY, $userEntitySerialized)
                || !$this->redis->hSet($this->userSessionHash, UserService::SESSION_TIME_TOUCH_KEY, time())) {
                return false;
            }
        } catch (\RedisException $e) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() ' . $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function setSessionHash(): bool
    {
        if (!$this->isSessionHashManually && $this->setSessionHashFromHttpHeader()) {
            return true;
        }
        if (!$this->isSessionHashManually && $this->cookiesEnabled && $this->setSessionHashFromCookie()) {
            return true;
        }
        if ($this->isSessionHashManually) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    protected function setSessionHashFromHttpHeader(): bool
    {
        $headers = array_change_key_case(\getallheaders(), CASE_UPPER); // server make it different
        if (!$headers || !isset($headers[UserService::SESSION_KEY])) {
            return false;
        }
        $this->userSessionHash = $headers[UserService::SESSION_KEY];
        if (empty($this->userSessionHash)) {
            return false;
        }
        return true;
    }

    /**
     * The cookie is set after a successful login process.
     *
     * @return bool
     */
    protected function setSessionHashFromCookie(): bool
    {
        $headers = \getallheaders();
        if (!isset($headers['Cookie'])) {
            return false;
        }
        $cookies = explode(';', $headers['Cookie']);
        if (empty($cookies) || !is_array($cookies)) {
            return false;
        }
        foreach ($cookies as $cookie) {
            $cookieArr = explode('=', $cookie);
            if (!empty($cookieArr[0]) && !empty($cookieArr[1]) && trim($cookieArr[0]) == UserService::SESSION_KEY) {
                $this->userSessionHash = trim($cookieArr[1]);
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $userSessionHash
     */
    public function setSessionHashManually(string $userSessionHash): void
    {
        $this->userSessionHash = $userSessionHash;
        $this->isSessionHashManually = true;
    }

    /**
     * Called on every request in \Bitkorn\User\Controller\AbstractUserController
     *
     * @return bool
     */
    public function checkUserContainer(): bool
    {
        if (!$this->setSessionHash()) {
            return false;
        }
        try {
            $userEntitySerialized = $this->redis->hGet($this->userSessionHash, UserService::SESSION_USERENTITY_KEY);
        } catch (\RedisException $e) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() line ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
        if (empty($userEntitySerialized) || !($this->userEntity = unserialize($userEntitySerialized))) {
            return false;
        }
        try {
            $sessionTimeTouch = intval($this->redis->hGet($this->userSessionHash, UserService::SESSION_TIME_TOUCH_KEY));
        } catch (\RedisException $e) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() line ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
        if ($sessionTimeTouch + $this->sessionLifetime < time()) {
            return false;
        }
        try {
            $this->redis->hSet($this->userSessionHash, UserService::SESSION_TIME_TOUCH_KEY, time());
        } catch (\RedisException $e) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() line ' . __LINE__ . ' ' . $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param int $userRoleId
     * @return bool
     */
    public function checkUserRoleAccess(int $userRoleId): bool
    {
        if (!$this->checkUserContainer()) {
            return false;
        }
        return $this->userEntity->isUserInRole($userRoleId);
    }

    /**
     * @param string $userRoleAlias
     * @return bool
     */
    public function checkUserRoleAliasAccess(string $userRoleAlias): bool
    {
        if (!$this->checkUserContainer()) {
            return false;
        }
        return $this->userEntity->isUserInRoleAlias($userRoleAlias);
    }

    /**
     * @param int $userRoleId
     * @return bool
     */
    public function checkUserRoleAccessMin(int $userRoleId): bool
    {
        if (!$this->checkUserContainer()) {
            return false;
        }
        return $this->userEntity->isUserInRoleMin($userRoleId);
    }

    /**
     * @param string $userRoleAlias
     * @return bool
     */
    public function checkUserRoleAliasAccessMin(string $userRoleAlias): bool
    {
        if (!$this->checkUserContainer() || !in_array($userRoleAlias, $this->userRoleIdAssoc)) {
            return false;
        }
        $flipped = array_flip($this->userRoleIdAssoc);
        return $this->checkUserRoleAccessMin($flipped[$userRoleAlias]);
    }

    /**
     * @param string $userRoleSpecialAlias
     * @return bool
     */
    public function hasUserRoleSpecial(string $userRoleSpecialAlias): bool
    {
        if (!$this->checkUserContainer()) {
            return false;
        }
        return $this->userEntity->hasUserRoleSpecial($userRoleSpecialAlias);
    }

    /**
     * @param string $userGroupAlias
     * @return bool
     */
    public function isUserInGroup(string $userGroupAlias): bool
    {
        if (!$this->checkUserContainer()) {
            return false;
        }
        return $this->userEntity->isUserInGroup($userGroupAlias);
    }

    /**
     * @param string $userRightAlias
     * @return bool
     */
    public function hasUserRight(string $userRightAlias): bool
    {
        if (!$this->checkUserContainer()) {
            return false;
        }
        return $this->userEntity->hasUserRight($userRightAlias);
    }

    /**
     * @param string $method For REST resourcen.
     * @return bool
     */
    public function checkRightsnroles(string $method = ''): bool
    {
        if (!$this->checkUserContainer()) {
            return false;
        }
        return $this->rightsnroles->hasAccess($method, $this->userEntity->getUserRole(), $this->userEntity->getUserGroups(), $this->userEntity->getUserRights());
    }

    /**
     * Delete Redis session hash & unlink UserEntity
     * @todo Delete redis entry (not only the values) ...for a clean redis db.
     */
    public function logout(): void
    {
        $this->userEntity = new UserEntity();
        if(isset($this->userSessionHash)) {
            try {
                $this->redis->hSet($this->userSessionHash, UserService::SESSION_TIME_TOUCH_KEY, '0');
                $this->redis->hSet($this->userSessionHash, UserService::SESSION_USERENTITY_KEY, '');
            } catch (\RedisException $e) {
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() ' . $e->getMessage());
            }
        }
    }

    /**
     * @return int
     */
    public function countUsers(): int
    {
        return $this->userTable->countUsers();
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUserDetails(string $userUuid): array
    {
        return $this->userTable->getUserDetails($userUuid);
    }

    /**
     * @param string $login
     * @return array
     */
    public function getUserByLogin(string $login): array
    {
        return $this->userTable->getUserByLogin($login);
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUserWithRightsnRoles(string $userUuid): array
    {
        $user = $this->userTable->getUserDetails($userUuid);
        $userEntity = new UserEntity();
        if (!$userEntity->loadWithUserData($user)) {
            $this->logger->debug(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
        }
        if (!$userEntity->loadUserRole($this->userRoleTable)) {
            $this->logger->debug(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
        }
        if (!$userEntity->loadUserRoleSpecialRelations($this->userRoleSpecialRelationTable)) {
            $this->logger->debug(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
        }
        if (!$userEntity->loadUserRightRelations($this->userRightRelationTable)) {
            $this->logger->debug(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
        }
        if (!$userEntity->loadUserGroupRelations($this->userGroupRelationTable)) {
            $this->logger->debug(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
        }
        return ArrayUtils::merge($userEntity->getStorage(), $userEntity->getRightsnRoles());
    }

    /**
     * @param string $userUuid
     * @param string $userPasswd
     * @return bool
     */
    public function updateUserPasswd(string $userUuid, string $userPasswd): bool
    {
        return $this->userTable->updatePassword($userUuid, $this->computePasswordHash($userPasswd)) >= 0;
    }

    /**
     * @param string $code
     * @param string $passwd
     * @return bool
     */
    public function updateUserPasswdWithCode(string $code, string $passwd): bool
    {
        $user = $this->userTable->getUserByPasswdForgotHash($code);
        if (!isset($user)) {
            return false;
        }
        if ($this->userTable->updatePasswordWithForgotPasswdHash($code, $this->computePasswordHash($passwd)) < 1) {
            return false;
        }
        if ($this->userTable->updatePasswordForgotHash($user['user_uuid'], '') == 1) {
            return true;
        }
        return false;
    }

    /**
     * @param string $userUuid
     * @param int $userActive
     * @return bool
     */
    public function updateUserActive(string $userUuid, int $userActive): bool
    {
        return $this->userTable->updateActive($userUuid, $userActive) >= 0;
    }

    /**
     * @param string $userUuid
     * @param string $userEmail
     * @return bool
     */
    public function updateUserEmail(string $userUuid, string $userEmail): bool
    {
        return $this->userTable->updateEmail($userUuid, $userEmail) >= 0;
    }

    /**
     * @param string $userUuid
     * @param int $userRoleId
     * @return string
     * @deprecated Weil es nur eine Role pro User gibt - ist das hier kakke
     */
    public function setUserRoleRelation(string $userUuid, int $userRoleId): string
    {
        return $this->userRoleRelationTable->insertUserRoleRelation($userUuid, $userRoleId);
    }

    /**
     * @param string $userUuid
     * @param int $userRoleId
     * @return bool
     * @deprecated Weil es eine Role pro User geben muss - ist das hier kakke
     */
    public function deleteUserRoleRelation(string $userUuid, int $userRoleId): bool
    {
        return $this->userRoleRelationTable->deleteUserRoleRelation($userUuid, $userRoleId) > 0;
    }

    /**
     * @param string $userUuid
     * @return bool
     */
    public function switchUserActive(string $userUuid): bool
    {
        $user = $this->userTable->getUser($userUuid);
        if (empty($user)) {
            return false;
        }
        if ($user['user_active'] == 1) {
            $userActive = 0;
        } else {
            $userActive = 1;
        }
        return $this->userTable->updateActive($userUuid, $userActive) >= 0;
    }

    /**
     * @param bool $onlyActive
     * @param bool $asKeyValObj
     * @return array
     */
    public function getUserUuidAssoc(bool $onlyActive = false, bool $asKeyValObj = false): array
    {
        return $this->userTable->getUserUuidAssoc($onlyActive, $asKeyValObj);
    }

    /**
     * @param string $userUuid
     * @param string $passwd
     * @return bool
     */
    public function checkUserPassword(string $userUuid, string $passwd): bool
    {
        $userData = $this->userTable->getUser($userUuid);
        if (empty($userData)) {
            return false;
        }
        return $this->computePasswordHash($passwd) == $userData['user_passwd'];
    }

    /**
     * @param string $search
     * @param bool $onlyActive
     * @return array
     */
    public function searchUser(string $search, bool $onlyActive = false): array
    {
        return $this->userTable->searchUser($search, $onlyActive);
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->userRoleTable->getUserRoles();
    }

    /**
     * @param UserFormEntity $userFormEntity
     * @return bool
     */
    public function updateUserWithUserFormEntity(UserFormEntity $userFormEntity): bool
    {
        return $this->userTable->updateUserWithUserFormEntity($userFormEntity) >= 0;
    }
}
