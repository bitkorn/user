<?php

namespace Bitkorn\User\Service\Group;

use Bitkorn\User\Table\User\Group\UserGroupRelationTable;
use Bitkorn\User\Table\User\Group\UserGroupTable;
use Bitkorn\Trinket\Service\AbstractService;

class UserGroupRelationService extends AbstractService
{

    protected UserGroupTable $userGroupTable;
    protected UserGroupRelationTable $userGroupRelationTable;

    public function setUserGroupRelationTable(UserGroupRelationTable $userGroupRelationTable): void
    {
        $this->userGroupRelationTable = $userGroupRelationTable;
    }

    public function setUserGroupTable(UserGroupTable $userGroupTable): void
    {
        $this->userGroupTable = $userGroupTable;
    }

    /**
     * @return array
     */
    public function getUserGroupRelations(): array
    {
        return $this->userGroupRelationTable->getUserGroupRelations();
    }

    /**
     * @param string $userUuid
     * @param string $userGroupUuid
     * @return string
     */
    public function insertUserGroupRelation(string $userUuid, string $userGroupUuid): string
    {
        if (($uuid = $this->userGroupRelationTable->getUserGroupRelationUuid($userUuid, $userGroupUuid))) {
            return $uuid;
        }
        return $this->userGroupRelationTable->insertUserGroupRelation($userUuid, $userGroupUuid);
    }

    /**
     * @param string $userGroupRelationUuid
     * @return bool
     */
    public function existUserGroupRelation(string $userGroupRelationUuid): bool
    {
        return !empty($this->userGroupRelationTable->getUserGroupRelation($userGroupRelationUuid));
    }

    /**
     * @param string $userGroupRelationUuid
     * @return bool
     */
    public function deleteUserGroupRelation(string $userGroupRelationUuid): bool
    {
        return $this->userGroupRelationTable->deleteUserGroupRelation($userGroupRelationUuid) > 0;
    }

    /**
     * @param string $userUuid
     * @param string $userGroupUuid
     * @return bool
     */
    public function deleteUserGroupRelationWithUuids(string $userUuid, string $userGroupUuid): bool
    {
        return $this->userGroupRelationTable->deleteUserGroupRelationWithUuids($userUuid, $userGroupUuid) > 0;
    }

}
