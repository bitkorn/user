<?php

namespace Bitkorn\User\Factory\Service\Group;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\User\Service\Group\UserGroupRelationService;
use Bitkorn\User\Table\User\Group\UserGroupRelationTable;
use Bitkorn\User\Table\User\Group\UserGroupTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserGroupRelationServiceFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserGroupRelationService();
        $service->setUserGroupRelationTable($container->get(UserGroupRelationTable::class));
        $service->setUserGroupTable($container->get(UserGroupTable::class));
        return $service;
    }
}
