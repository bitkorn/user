<?php

namespace Bitkorn\User\Table\User;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class ViewUserRightRelationTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_user_right_relation';

    public function getUsersWithRight(string $userRightAlias): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_right_alias' => $userRightAlias]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
