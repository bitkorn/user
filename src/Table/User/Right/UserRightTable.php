<?php

namespace Bitkorn\User\Table\User\Right;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class UserRightTable extends AbstractLibTable
{
    protected $table = 'user_right';

    public function getUserRightIds(): array
    {
        $select = $this->sql->select();
        $ids = [];
        try {
            $select->columns(['user_right_id']);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    $ids[] = $row['user_right_id'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $ids;
    }

    public function getUserRightIdAssoc(): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArray = $result->toArray();
                foreach ($resultArray as $resultRow) {
                    $idAssoc[$resultRow['user_right_id']] = $resultRow['user_right_alias'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

    /**
     * @return array From view_user_right with additional field 'user_uuids_csv'.
     */
    public function getUserRightsWithUserUuids(): array
    {
        $select = new Select('view_user_right');
        try {
            $select->order('user_right_alias');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
