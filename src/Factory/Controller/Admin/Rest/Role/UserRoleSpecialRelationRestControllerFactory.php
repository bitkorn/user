<?php

namespace Bitkorn\User\Factory\Controller\Admin\Rest\Role;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\User\Controller\Admin\Rest\Role\UserRoleSpecialRelationRestController;
use Bitkorn\User\Service\UserService;
use Bitkorn\User\Service\Role\UserRoleSpecialRelationService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserRoleSpecialRelationRestControllerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new UserRoleSpecialRelationRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setUserRoleSpecialRelationService($container->get(UserRoleSpecialRelationService::class));
        return $controller;
    }
}
