<?php

namespace Bitkorn\User\Factory\Service\Role;

use Bitkorn\User\Service\Role\UserRoleService;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserRoleServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserRoleService();
        $service->setLogger($container->get('logger'));
        $service->setUserRoleTable($container->get(UserRoleTable::class));
        return $service;
    }
}
