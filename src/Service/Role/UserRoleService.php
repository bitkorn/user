<?php

namespace Bitkorn\User\Service\Role;

use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\User\Table\User\Role\UserRoleTable;

class UserRoleService extends AbstractService
{
    protected UserRoleTable $userRoleTable;

    public function setUserRoleTable(UserRoleTable $userRoleTable): void
    {
        $this->userRoleTable = $userRoleTable;
    }

    /**
     * @return array db.user_role
     */
    public function getUserRoles(): array
    {
        return $this->userRoleTable->getUserRoles();
    }
}
