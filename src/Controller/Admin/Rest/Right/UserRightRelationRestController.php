<?php

namespace Bitkorn\User\Controller\Admin\Rest\Right;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Bitkorn\User\Service\Right\UserRightRelationService;
use Laminas\Http\Response;
use Laminas\Validator\Digits;
use Laminas\Validator\Uuid;

class UserRightRelationRestController extends AbstractUserRestController
{
    protected UserRightRelationService $userRightRelationService;

    public function setUserRightRelationService(UserRightRelationService $userRightRelationService): void
    {
        $this->userRightRelationService = $userRightRelationService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($data['user_uuid']) || !(new Digits())->isValid($data['user_right_id'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (empty($userRightRelationUuid = $this->userRightRelationService->insertUserRightRelation($data['user_uuid'], $data['user_right_id']))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setUuid($userRightRelationUuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id Without query param 'user_right_id' the $id is `user_right_relation_uuid` ELSE it is the `user_uuid`.
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $userRightId = $this->params()->fromQuery('user_right_id');
        if (!(new Uuid())->isValid($id) || (!empty($userRightId) && !(new Digits())->isValid($userRightId))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($userRightId && $this->userRightRelationService->deleteUserRightRelationWithKeys($id, $userRightId)) {
            $jsonModel->setSuccess(1);
        } else if ($this->userRightRelationService->deleteUserRightRelation($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
