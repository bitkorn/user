<?php

namespace Bitkorn\User\Controller\Admin\Rest\Role;

use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\User\Controller\AbstractUserRestController;
use Bitkorn\User\Service\Role\UserRoleSpecialRelationService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

/**
 * Class UserRoleSpecialRelationController
 * @package Bitkorn\User\Controller\Admin\Rest\User\Role
 */
class UserRoleSpecialRelationRestController extends AbstractUserRestController
{
    protected UserRoleSpecialRelationService $userRoleSpecialRelationService;

    public function setUserRoleSpecialRelationService(UserRoleSpecialRelationService $userRoleSpecialRelationService): void
    {
        $this->userRoleSpecialRelationService = $userRoleSpecialRelationService;
    }

    /**
     *
     * # GET /user-admin-user-role-special-relation
     *
     * Get ALL user_role_special_relations with all data (user_role_special & user_role).
     *
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "data": [
     *                     {
     *                         "user_role_special_relation_uuid": "f5d791df-e4e0-11e8-ab5a-7824afbed67e",
     *                         "user_role_special_uuid": "f5c3e6cf-e4e0-11e8-ab5a-7824afbed67e",
     *                         "user_role_id": "1",
     *                         "user_role_special_relation_value": "1",
     *                         "user_role_special_alias": "next test",
     *                         "user_role_special_name": "''",
     *                         "user_role_special_desc": "Einfach mal wieder n Test",
     *                         "user_role_special_time_create": "1541851548",
     *                         "user_role_special_order_priority": "5",
     *                         "user_role_alias": "god",
     *                         "user_role_customizable": "0",
     *                         "user_role_desc": ""
     *                     },
     *                     {
     *                         ...
     *                     }
     *                 ],
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: If available, the requested JSON array with data objects
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 403 insufficient permissions (min user role 10)
     *     - 200 success
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccess(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            $jsonModel->addMessage(AbstractUserController::MESSAGE_ONLY_GOD);
            return $jsonModel;
        }
        if (!$this->userRoleSpecialRelationService->loadUserRoleSpecialRelationsAll()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            $jsonModel->addMessage(AbstractUserController::MESSAGE_ERROR_WHILE_SELECT_DATA);
            return $jsonModel;
        }
        $jsonModel->setVariable('userRoleSpecialRelationEntities', $this->userRoleSpecialRelationService->getUserRoleSpecialRelationEntitiesData());
        return $jsonModel;
    }

    /**
     * # PUT /user-admin-user-role-special-relation/:uuid
     *
     * Update one user_role_special_relation_value
     *
     * @param string $id URL Parameter
     * @param array $data **x-www-form-urlencoded**
     *     - **user_role_special_relation_value**: 1 or 0
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "data": [],
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: An empty array
     *     - **messages**: only in development mode. E.g. one or more messages from input validation
     * ### HTTP
     *     - 403 insufficient permissions (min user role 10)
     *     - 200 success data update
     *     - 202 success but nothing to update
     *     - 204 Data not found
     *     - 400 not all values are valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccess(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            $jsonModel->addMessage(AbstractUserController::MESSAGE_ONLY_GOD);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->userRoleSpecialRelationService->existUserRoleSpecialRelation($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_204);
            return $jsonModel;
        }

        if (!isset($data['user_role_special_relation_value'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $value = (int)$data['user_role_special_relation_value'];

        $result = $this->userRoleSpecialRelationService->updateUserRoleSpecialRelationValue($id, $value);
        switch ($result) {
            case -1:
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
                break;
            case 0:
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_202);
                $jsonModel->setSuccess(1);
                break;
            case 1:
                $jsonModel->setSuccess(1);
                break;
        }
        return $jsonModel;
    }
}
