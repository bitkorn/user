<?php

namespace Bitkorn\User\Table\User\Group;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class UserGroupRelationTable extends AbstractLibTable
{
    protected $table = 'user_group_relation';

    /**
     * @return array
     */
    public function getUserGroupRelations(): array
    {
        $select = $this->sql->select();
        try {

            $select->join('user_group', 'user_group.user_group_uuid = user_group_relation.user_group_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('user', 'user.user_uuid = user_group_relation.user_uuid', ['user_login', 'user_role_id', 'user_active', 'user_time_create'], Select::JOIN_LEFT);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUserGroupsByUserUuid(string $userUuid): array
    {
        $select = $this->sql->select();
        try {

            $select->join('user_group', 'user_group.user_group_uuid = user_group_relation.user_group_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where(['user_group_relation.user_uuid' => $userUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getUserGroupRelationUuid(string $userUuid, string $userGroupUuid): string
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'user_uuid'       => $userUuid,
                'user_group_uuid' => $userGroupUuid
            ]);
            $result = $this->selectWith($select);
            /** @var HydratingResultSet $result */
            if ($result->valid() && $result->count() > 0) {
                $current = $result->current();
                return $current['user_group_relation_uuid'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $userGroupRelationUuid
     * @return array
     */
    public function getUserGroupRelation(string $userGroupRelationUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'user_group_relation_uuid' => $userGroupRelationUuid
            ]);
            $result = $this->selectWith($select);
            /** @var HydratingResultSet $result */
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $userUuid
     * @param string $userGroupUuid
     * @return string
     */
    public function insertUserGroupRelation(string $userUuid, string $userGroupUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'user_group_relation_uuid' => $uuid,
                'user_uuid'                => $userUuid,
                'user_group_uuid'          => $userGroupUuid
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $userGroupUuid
     * @return int
     */
    public function deleteForUserGroup(string $userGroupUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_group_uuid' => $userGroupUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userGroupRelationUuid
     * @return int
     */
    public function deleteUserGroupRelation(string $userGroupRelationUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_group_relation_uuid' => $userGroupRelationUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteUserGroupRelationWithUuids(string $userUuid, string $userGroupUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_uuid' => $userUuid, 'user_group_uuid' => $userGroupUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function deleteForUser(string $userUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_uuid' => $userUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
