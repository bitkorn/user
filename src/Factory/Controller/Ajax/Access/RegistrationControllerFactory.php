<?php


namespace Bitkorn\User\Factory\Controller\Ajax\Access;


use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\User\Controller\Ajax\Access\RegistrationController;
use Bitkorn\User\Form\User\UserForm;
use Bitkorn\User\Service\UserService;
use Bitkorn\User\Service\Registration\UserRegistrationService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RegistrationControllerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new RegistrationController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setUserForm($container->get(UserForm::class));
        $config = $container->get('config');
        $controller->setConfigUser($config['bitkorn_user']);
        $controller->setUserRegistrationService($container->get(UserRegistrationService::class));
        return $controller;
    }
}
