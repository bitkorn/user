<?php

namespace Bitkorn\User\Controller\Admin\Rest\Role;

use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\User\Form\User\Role\UserRoleSpecialForm;
use Bitkorn\User\Service\Role\UserRoleSpecialService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\Response;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;

class UserRoleSpecialController extends AbstractUserRestController
{
    protected UserRoleSpecialForm $userRoleSpecialForm;
    protected UserRoleSpecialService $userRoleSpecialService;

    public function setUserRoleSpecialForm(UserRoleSpecialForm $userRoleSpecialForm): void
    {
        $this->userRoleSpecialForm = $userRoleSpecialForm;
    }

    public function setUserRoleSpecialService(UserRoleSpecialService $userRoleSpecialService): void
    {
        $this->userRoleSpecialService = $userRoleSpecialService;
    }

    /**
     * # POST /user-user-role-special
     *
     * Create an user_role_special with all user_role_special_relations for each user_role.
     *
     * @param array $data **x-www-form-urlencoded**
     *     - **user_role_special_alias**: must be unique in db
     *     - **user_role_special_desc**: some desc
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "data": {
     *                     "user_role_special_uuid": "12345678-17e4-11e9-8e49-7824afbed67e"
     *                 },
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: If available, the new UUID from the generated record
     *     - **messages**: only in development mode. E.g. one or more messages from input validation
     * ### HTTP
     *     - 403 insufficient permissions (min user role 10)
     *     - 201 success created
     *     - 400 not all values are valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccess(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            $jsonModel->addMessage(AbstractUserController::MESSAGE_ONLY_GOD);
            return $jsonModel;
        }

        $this->userRoleSpecialForm->setUserRoleSpecialUuidAvailable(false);
        $this->userRoleSpecialForm->init();
        $this->userRoleSpecialForm->setData($data);
        if (!$this->userRoleSpecialForm->isValid()) {
            $jsonModel->addMessages($this->userRoleSpecialForm->getMessages());
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if (!$this->userRoleSpecialService->loadUserRoleSpecialEntity($this->userRoleSpecialForm->getData())) {
            $jsonModel->addMessage(AbstractUserController::MESSAGE_CAN_NOT_LOAD_ENTITY);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if ($this->userRoleSpecialService->existUserRoleSpecialAlias()) {
            $jsonModel->addMessage('UserRoleSpecialAlias must be unique.');
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if (empty($userRoleSpecialUuid = $this->userRoleSpecialService->insertNewUserRoleSpecial())) {
            $jsonModel->addMessage(AbstractUserController::MESSAGE_ERROR_WHILE_INSERT_DATA);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }

        $jsonModel->setSuccess(1);
        $jsonModel->setVariable('user_role_special_uuid', $userRoleSpecialUuid);
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        return $jsonModel;
    }

    /**
     * # DELETE /user-user-role-special/:uuid
     *
     * Delete an user_role_special with all user_role_special_relations for each user_role.
     *
     * @param string $id UserRoleSpecial UUID
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "data": [],
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: An empty JSON array.
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 403 insufficient permissions (user role 1)
     *     - 200 success
     *     - 204 Data not found
     *     - 400 not all values are valid OR the alias isn't valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccess(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            $jsonModel->addMessage(AbstractUserController::MESSAGE_ONLY_GOD);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        $result = $this->userRoleSpecialService->deleteUserRoleSpecial($id);
        switch ($result) {
            case -1:
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
                break;
            case 0:
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_204);
                $jsonModel->setSuccess(1);
                break;
            case 1:
                $jsonModel->setSuccess(1);
                break;
        }

        return $jsonModel;
    }

}
