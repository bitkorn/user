<?php

namespace Bitkorn\User\Controller\Admin\Rest\Group;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Bitkorn\User\Service\Group\UserGroupRelationService;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class UserGroupRelationRestController extends AbstractUserRestController
{
    protected UserGroupRelationService $userGroupRelationService;

    public function setUserGroupRelationService(UserGroupRelationService $userGroupRelationService): void
    {
        $this->userGroupRelationService = $userGroupRelationService;
    }

    /**
     * # POST /user-user-group-relation
     *
     * Create a new userGroupRelation between a user and a userGroup.
     *
     * @param array $data **x-www-form-urlencoded**
     *     - **user_uuid**: valid user UUID
     *     - **user_group_uuid**: valid userGroup UUID
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: An empty JSON array
     *     - **messages**: only in development mode. E.g. one or more messages from input validation
     * ### HTTP
     *     - 403 insufficient permissions (min user role 3)
     *     - 201 success created
     *     - 400 not all values are valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        if (!(new Uuid())->isValid($data['user_uuid']) || !(new Uuid())->isValid($data['user_group_uuid'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if (empty($userGroupRelationUuid = $this->userGroupRelationService->insertUserGroupRelation($data['user_uuid'], $data['user_group_uuid']))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setVariable('user_group_relation_uuid', $userGroupRelationUuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * # DELETE /user-user-group-relation/:uuid
     *
     * Delete a userGroupRelation
     *
     * @param string $id UserGroupRelation UUID
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: An empty JSON array.
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 403 insufficient permissions (min user role 3)
     *     - 200 success
     *     - 204 Data not found
     *     - 400 not all values are valid OR the alias isn't valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->userGroupRelationService->existUserGroupRelation($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_204);
            return $jsonModel;
        }

        if (!$this->userGroupRelationService->deleteUserGroupRelation($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        } else {
            $jsonModel->setSuccess(1);
        }

        return $jsonModel;
    }

    /**
     * # GET /user-user-group-relation
     *
     * Get a list of userGroupRelations.
     *
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "userGroupRelations": [
     *                     {
     *                         "user_group_relation_uuid": "cb085759-e738-11e8-bc3f-7824afbed67e",
     *                         "user_uuid": "8b46e058-e441-11e8-b4c9-7824afbed67e",
     *                         "user_group_uuid": "637f857b-e408-11e8-b764-7824afbed67e",
     *                         "user_group_name": "Testgruppe",
     *                         "user_group_desc": "only for test",
     *                         "user_login": "rumpelstielz",
     *                         "user_role_id": "2",
     *                         "user_active": "1",
     *                         "user_time_create": "1541783079"
     *                     },
     *                     {
     *                         ...
     *                     }
     *                 ],
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: If available, the requested JSON array with data objects
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 403 insufficient permissions (min user role 3)
     *     - 200 success
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $userGroupRelations = $this->userGroupRelationService->getUserGroupRelations();
        if (isset($userGroupRelations)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        $jsonModel->setVariable('userGroupRelations', $userGroupRelations);
        return $jsonModel;
    }
}
