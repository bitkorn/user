<?php

namespace Bitkorn\User\Factory\Service\Registration;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Bitkorn\User\Service\Registration\UserRegistrationService;
use Bitkorn\User\Table\User\UserTable;
use Bitkorn\Mail\Mail\Draft\DraftManager;
use Bitkorn\Mail\Mail\MailWrapper;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;

class UserRegistrationServiceFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserRegistrationService();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setUserConfig($config['bitkorn_user']);
        $service->setUserTable($container->get(UserTable::class));
        $service->setMailWrapper($container->get(MailWrapper::class));
        $service->setDraftManager($container->get(DraftManager::class));
        /** @var HelperPluginManager $vhm */
        $vhm = $container->get('ViewHelperManager');
        $service->setUrlHelper($vhm->get('url'));
        $service->setServerUrlHelper($vhm->get('ServerUrl'));
        $service->setUserService($container->get(UserService::class));
        return $service;
    }
}
