<?php

namespace Bitkorn\User;

use Bitkorn\User\Controller\Admin\Ajax\UserGroupManagementController;
use Bitkorn\User\Controller\Admin\Ajax\UserManagementController;
use Bitkorn\User\Controller\Admin\Ajax\UserSearchController;
use Bitkorn\User\Controller\Admin\Rest\Group\UserGroupRelationRestController;
use Bitkorn\User\Controller\Admin\Rest\Group\UserGroupRestController;
use Bitkorn\User\Controller\Admin\Rest\Right\UserRightRelationRestController;
use Bitkorn\User\Controller\Admin\Rest\Right\UserRightRestController;
use Bitkorn\User\Controller\Admin\Rest\Role\UserRoleRelationRestController;
use Bitkorn\User\Controller\Ajax\Access\PasswordController;
use Bitkorn\User\Controller\Ajax\Access\RegistrationController;
use Bitkorn\User\Controller\Ajax\ListsController;
use Bitkorn\User\Factory\Controller\Admin\Ajax\UserGroupManagementControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Ajax\UserManagementControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Ajax\UserSearchControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Rest\Group\UserGroupRelationRestControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Rest\Group\UserGroupRestControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Rest\Right\UserRightRelationRestControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Rest\Right\UserRightRestControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Rest\Role\UserRoleRelationRestControllerFactory;
use Bitkorn\User\Factory\Controller\Ajax\Access\AuthenticationControllerFactory;
use Bitkorn\User\Controller\Ajax\Access\AuthenticationController;
use Bitkorn\User\Controller\Admin\Rest\Role\UserRoleSpecialController;
use Bitkorn\User\Controller\Admin\Rest\Role\UserRoleSpecialRelationRestController;
use Bitkorn\User\Controller\Admin\Rest\UserRestController;
use Bitkorn\User\Factory\Controller\Ajax\Access\PasswordControllerFactory;
use Bitkorn\User\Factory\Controller\Ajax\Access\RegistrationControllerFactory;
use Bitkorn\User\Factory\Controller\Ajax\ListsControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Rest\Role\UserRoleSpecialControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Rest\Role\UserRoleSpecialRelationRestControllerFactory;
use Bitkorn\User\Factory\Controller\Admin\Rest\UserRestControllerFactory;
use Bitkorn\User\Factory\Form\User\Group\UserGroupFormFactory;
use Bitkorn\User\Factory\Service\Group\UserGroupServiceFactory;
use Bitkorn\User\Factory\Service\Registration\UserRegistrationServiceFactory;
use Bitkorn\User\Factory\Service\Right\UserRightRelationServiceFactory;
use Bitkorn\User\Factory\Service\Right\UserRightServiceFactory;
use Bitkorn\User\Factory\Service\RightsnrolesRoutesServiceFactory;
use Bitkorn\User\Factory\Service\Role\UserRoleServiceFactory;
use Bitkorn\User\Factory\View\Helper\IsUserInRoleAliasFactory;
use Bitkorn\User\Form\User\Group\UserGroupForm;
use Bitkorn\User\Service\Group\UserGroupService;
use Bitkorn\User\Service\Registration\UserRegistrationService;
use Bitkorn\User\Service\Right\UserRightRelationService;
use Bitkorn\User\Service\Right\UserRightService;
use Bitkorn\User\Service\RightsnrolesRoutesService;
use Bitkorn\User\Service\Role\UserRoleService;
use Bitkorn\User\View\Helper\IsUserInRoleAlias;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Bitkorn\User\Factory\Form\User\Role\UserRoleSpecialFormFactory;
use Bitkorn\User\Factory\Form\User\UserFormFactory;
use Bitkorn\User\Factory\Service\UserServiceFactory;
use Bitkorn\User\Factory\Service\Group\UserGroupRelationServiceFactory;
use Bitkorn\User\Factory\Service\Role\UserRoleRelationServiceFactory;
use Bitkorn\User\Factory\Service\Role\UserRoleSpecialRelationServiceFactory;
use Bitkorn\User\Factory\Service\Role\UserRoleSpecialServiceFactory;
use Bitkorn\User\Factory\Table\User\Group\UserGroupRelationTableFactory;
use Bitkorn\User\Factory\Table\User\Group\UserGroupTableFactory;
use Bitkorn\User\Factory\Table\User\Right\UserRightRelationTableFactory;
use Bitkorn\User\Factory\Table\User\Right\UserRightTableFactory;
use Bitkorn\User\Factory\Table\User\Role\UserRoleRelationTableFactory;
use Bitkorn\User\Factory\Table\User\Role\UserRoleSpecialTableFactory;
use Bitkorn\User\Factory\Table\User\Role\UserRoleSpecialRelationTableFactory;
use Bitkorn\User\Factory\Table\User\Role\UserRoleTableFactory;
use Bitkorn\User\Factory\Table\User\UserTableFactory;
use Bitkorn\User\Form\User\Role\UserRoleSpecialForm;
use Bitkorn\User\Form\User\UserForm;
use Bitkorn\User\Service\UserService;
use Bitkorn\User\Service\Group\UserGroupRelationService;
use Bitkorn\User\Service\Role\UserRoleRelationService;
use Bitkorn\User\Service\Role\UserRoleSpecialRelationService;
use Bitkorn\User\Service\Role\UserRoleSpecialService;
use Bitkorn\User\Table\User\Group\UserGroupRelationTable;
use Bitkorn\User\Table\User\Group\UserGroupTable;
use Bitkorn\User\Table\User\Right\UserRightRelationTable;
use Bitkorn\User\Table\User\Right\UserRightTable;
use Bitkorn\User\Table\User\Role\UserRoleRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleSpecialTable;
use Bitkorn\User\Table\User\Role\UserRoleSpecialRelationTable;
use Bitkorn\User\Table\User\Role\UserRoleTable;
use Bitkorn\User\Table\User\UserTable;

return [
    'router'          => [
        'routes' => [
            /*
             * Ajax - access
             */
            'bitkorn_user_ajax_access_registration_register'                           => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/register',
                    'defaults' => [
                        'controller' => RegistrationController::class,
                        'action'     => 'register',
                    ],
                ],
            ],
            'bitkorn_user_ajax_access_registration_registerverify'                     => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/ajax-register-verify',
                    'defaults' => [
                        'controller' => RegistrationController::class,
                        'action'     => 'registerVerify',
                    ],
                ],
            ],
            'bitkorn_user_ajax_access_registration_passwordforgot'                     => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/password-forgot-request-code',
                    'defaults' => [
                        'controller' => RegistrationController::class,
                        'action'     => 'passwordForgotRequestCode',
                    ],
                ],
            ],
            'bitkorn_user_ajax_access_registration_passwordrenew'                      => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/password-renew-with-code',
                    'defaults' => [
                        'controller' => RegistrationController::class,
                        'action'     => 'passwordRenewWithCode',
                    ],
                ],
            ],
            'bitkorn_user_ajax_access_authentication_login'                            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => AuthenticationController::class,
                        'action'     => 'login',
                    ],
                ],
            ],
            'bitkorn_user_ajax_access_authentication_logout'                           => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => AuthenticationController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],
            'bitkorn_user_ajax_access_authentication_check'                            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/session-check',
                    'defaults' => [
                        'controller' => AuthenticationController::class,
                        'action'     => 'check',
                    ],
                ],
            ],
            'bitkorn_user_ajax_access_password_logout'                                 => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/user-passwd-update',
                    'defaults' => [
                        'controller' => PasswordController::class,
                        'action'     => 'updatePasswd',
                    ],
                ],
            ],
            /*
             * AJAX - lists
             */
            'bitkorn_user_ajax_lists_user'                                             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-lists-user-uuid-assoc[/:only_active]',
                    'constraints' => [
                        'only_active' => '[0-1]+',
                    ],
                    'defaults'    => [
                        'controller' => ListsController::class,
                        'action'     => 'userUuidAssoc',
                    ],
                ],
            ],
            'bitkorn_user_ajax_lists_userroles'                                        => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/user-lists-user-roles',
                    'defaults' => [
                        'controller' => ListsController::class,
                        'action'     => 'userRoles',
                    ],
                ],
            ],
            'bitkorn_user_ajax_lists_rightsnroles'                                        => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/user-lists-rightsnroles',
                    'defaults' => [
                        'controller' => ListsController::class,
                        'action'     => 'rightsnroles',
                    ],
                ],
            ],
            /*
             * Admin - REST
             */
            'bitkorn_user_rest_user_user'                                              => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-admin-user[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserRestController::class
                    ],
                ],
            ],
            /*
             * Admin - REST group
             */
            'bitkorn_user_admin_rest_group'                                            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-admin-group[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserGroupRestController::class
                    ],
                ],
            ],
            'bitkorn_user_admin_rest_grouprelation'                                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-admin-group-relation[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserGroupRelationRestController::class
                    ],
                ],
            ],
            /*
             * Admin - REST right
             */
            'bitkorn_user_admin_rest_right'                                            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-admin-right[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserRightRestController::class
                    ],
                ],
            ],
            'bitkorn_user_admin_rest_rightrelation'                                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-admin-right-relation[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserRightRelationRestController::class
                    ],
                ],
            ],
            /*
             * Admin - REST role
             */
            'bitkorn_user_admin_rest_role_userroleRelation'                            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-admin-role-relation[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserRoleRelationRestController::class
                    ],
                ],
            ],
            'bitkorn_user_rest_role_userrolespecial'                                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-user-role-special[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserRoleSpecialController::class
                    ],
                ],
            ],
            'bitkorn_user_admin_rest_role_userrolespecialrelation'                     => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-admin-role-special-relation[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserRoleSpecialRelationRestController::class
                    ],
                ],
            ],
            /*
             * Admin - AJAX
             */
            'bitkorn_user_admin_ajax_user_usermanagement_massdelete'                   => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/user-admin-ajax-user-massdelete',
                    'defaults' => [
                        'controller' => UserManagementController::class,
                        'action'     => 'massDelete'
                    ],
                ],
            ],
            'bitkorn_user_admin_ajax_user_usermanagement_switchactive'                 => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-admin-ajax-user-switch-active[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserManagementController::class,
                        'action'     => 'switchActive'
                    ],
                ],
            ],
            'bitkorn_user_admin_ajax_user_usergroupmanagement_deleteusergrouprelation' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/user-admin-ajax-delete-user-group-relation/:user_uuid/:group_uuid',
                    'constraints' => [
                        'user_uuid'  => '[0-9A-Fa-f-]+',
                        'group_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserGroupManagementController::class,
                        'action'     => 'deleteUserGroupRelation'
                    ],
                ],
            ],
            'bitkorn_user_admin_ajax_user_usersearch_searchsimple'                     => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/user-admin-ajax-user-searchsimple',
                    'defaults' => [
                        'controller' => UserSearchController::class,
                        'action'     => 'searchSimple'
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories' => [
            // AJAX
            RegistrationController::class                => RegistrationControllerFactory::class,
            AuthenticationController::class              => AuthenticationControllerFactory::class,
            PasswordController::class                    => PasswordControllerFactory::class,
            ListsController::class                       => ListsControllerFactory::class,
            // REST
            UserRestController::class                    => UserRestControllerFactory::class,
            UserRoleSpecialController::class             => UserRoleSpecialControllerFactory::class,
            // Admin - REST
            UserRoleRelationRestController::class        => UserRoleRelationRestControllerFactory::class,
            UserRoleSpecialRelationRestController::class => UserRoleSpecialRelationRestControllerFactory::class,
            UserGroupRelationRestController::class       => UserGroupRelationRestControllerFactory::class,
            UserGroupRestController::class               => UserGroupRestControllerFactory::class,
            UserRightRestController::class               => UserRightRestControllerFactory::class,
            UserRightRelationRestController::class       => UserRightRelationRestControllerFactory::class,
            // Admin - AJAX
            UserManagementController::class              => UserManagementControllerFactory::class,
            UserGroupManagementController::class         => UserGroupManagementControllerFactory::class,
            UserSearchController::class                  => UserSearchControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'invokables' => [
        ],
        'factories'  => [
            // Table
            UserTable::class                      => UserTableFactory::class,
            UserRoleTable::class                  => UserRoleTableFactory::class,
            UserRoleRelationTable::class          => UserRoleRelationTableFactory::class,
            UserRoleSpecialTable::class           => UserRoleSpecialTableFactory::class,
            UserRoleSpecialRelationTable::class   => UserRoleSpecialRelationTableFactory::class,
            UserGroupTable::class                 => UserGroupTableFactory::class,
            UserGroupRelationTable::class         => UserGroupRelationTableFactory::class,
            UserRightTable::class                 => UserRightTableFactory::class,
            UserRightRelationTable::class         => UserRightRelationTableFactory::class,
            // Service
            UserService::class                    => UserServiceFactory::class,
            UserRoleService::class                => UserRoleServiceFactory::class,
            UserRoleRelationService::class        => UserRoleRelationServiceFactory::class,
            UserRoleSpecialService::class         => UserRoleSpecialServiceFactory::class,
            UserRoleSpecialRelationService::class => UserRoleSpecialRelationServiceFactory::class,
            UserGroupService::class               => UserGroupServiceFactory::class,
            UserGroupRelationService::class       => UserGroupRelationServiceFactory::class,
            UserRightService::class               => UserRightServiceFactory::class,
            UserRightRelationService::class       => UserRightRelationServiceFactory::class,
            UserRegistrationService::class        => UserRegistrationServiceFactory::class,
            RightsnrolesRoutesService::class      => RightsnrolesRoutesServiceFactory::class,
            // Form (InputFilter)
            UserForm::class                       => UserFormFactory::class,
            UserGroupForm::class                  => UserGroupFormFactory::class,
            UserRoleSpecialForm::class            => UserRoleSpecialFormFactory::class,
        ]
    ],
    'view_helpers'    => [
        'aliases'    => [
            'isUserInRoleAlias' => IsUserInRoleAlias::class,
        ],
        'factories'  => [
            IsUserInRoleAlias::class => IsUserInRoleAliasFactory::class,
        ],
        'invokables' => [
        ],
    ],
    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'template_map'        => [
            // Email
            'template/email/registration/email-verify'         => __DIR__ . '/../view/template/email/registration/email-verify.phtml',
            'template/email/registration/password-forgot'      => __DIR__ . '/../view/template/email/registration/password-forgot.phtml',
            'template/email/registration/password-forgot-code' => __DIR__ . '/../view/template/email/registration/password-forgot-code.phtml',
        ],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'bitkorn_user'    => [
        'session_lifetime'          => 40000, // 86400 = 1 day; 31536000 = 1 year
        'user_role_id_default'      => 5,
        'registration_enabled'      => true,
        'registration_verify_route' => 'bitkorn_user_ajax_access_registration_registerverify', // used in UserRegistrationService
        'cookies_enabled'           => true, // if false then only HTTP header SESSIONHASH
        'brand_name'                => 'Brand Name',
        'brand_email'               => 'mail@example.com',
    ]
];
