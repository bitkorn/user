<?php

namespace Bitkorn\User\Factory\Service\Role;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\User\Service\Role\UserRoleRelationService;
use Bitkorn\User\Table\User\Role\UserRoleRelationTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserRoleRelationServiceFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserRoleRelationService();
        $service->setLogger($container->get('logger'));
        $service->setUserRoleRelationTable($container->get(UserRoleRelationTable::class));
        return $service;
    }
}
