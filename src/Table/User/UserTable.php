<?php

namespace Bitkorn\User\Table\User;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Bitkorn\User\Entity\User\UserFormEntity;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class UserTable extends AbstractLibTable
{
    /**
     * @var string
     */
    protected $table = 'user';

    /**
     * @param string $userUuid
     * @return array Data are only from table db.user.
     */
    public function getUser(string $userUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_uuid' => $userUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $hash
     * @return array
     */
    public function getUserByPasswdForgotHash(string $hash): array
    {
        if (empty($hash)) {
            return [];
        }
        $select = $this->sql->select();
        try {
            $select->where(['user_forgot_passwd_hash' => $hash]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() === 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param bool $onlyActive
     * @return array
     */
    public function getUsers(bool $onlyActive = true): array
    {
        $select = $this->sql->select();
        try {
            if ($onlyActive) {
                $select->where(['user_active' => 1]);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * Get user details, but not all db columns like user_passwd or user_registration_hash etc..
     *
     * @param string $userUuid
     * @return array
     */
    public function getUserDetails(string $userUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->columns(['user_uuid', 'user_login', 'user_time_create', 'user_email', 'user_lang_iso', 'user_time_update']);
            $select->where(['user_uuid' => $userUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $login
     * @return array
     */
    public function getUserByLogin(string $login): array
    {
        $select = $this->sql->select();
        try {
            $select->join('user_role_relation', 'user_role_relation.user_uuid = user.user_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('user_role', 'user_role.user_role_id = user_role_relation.user_role_id', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where
                ->equalTo('user_login', $login);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $email
     * @return array
     */
    public function getUserByEmail(string $email): array
    {
        $select = $this->sql->select();
        try {
            $select->join('user_role_relation', 'user_role_relation.user_uuid = user.user_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('user_role', 'user_role.user_role_id = user_role_relation.user_role_id', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where(['user_email' => $email]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function canLogin(string $login, string $password): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_login' => $login, 'user_passwd' => $password]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $login
     * @return bool
     */
    public function existUserLogin(string $login): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_login' => $login]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $email
     * @return bool
     */
    public function existUserEmail(string $email): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_email' => $email]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $userUuid
     * @return bool
     */
    public function existUser(string $userUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_uuid' => $userUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $login
     * @param string $passwordHash
     * @param int $active
     * @param string $email
     * @param string $lang
     * @return string UUID
     */
    public function insertUser(string $login, string $passwordHash, int $active, string $email, string $lang): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'user_uuid'        => $uuid,
                'user_login'       => $login,
                'user_passwd'      => $passwordHash,
                'user_active'      => $active,
                'user_time_create' => time(),
                'user_email'       => $email,
                'user_lang_iso'    => $lang,
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $userUuid
     * @param string $registrationHash
     * @return int
     */
    public function updateRegistrationHash(string $userUuid, string $registrationHash): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_registration_hash' => $registrationHash]);
            $update->where(['user_uuid' => $userUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userUuid
     * @param string $hash
     * @return int
     */
    public function updatePasswordForgotHash(string $userUuid, string $hash): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_forgot_passwd_hash' => $hash]);
            $update->where(['user_uuid' => $userUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $registrationHash
     * @return bool
     */
    public function verifyRegistration(string $registrationHash): bool
    {
        $select = $this->sql->select();
        $update = $this->sql->update();
        try {
            $select->where(['user_registration_hash' => $registrationHash]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if (!$result->valid() || $result->count() != 1) {
                return false;
            }
            $current = $result->current();
            if (empty($current['user_uuid'])) {
                return false;
            }
            $update->set(['user_registration_hash' => '', 'user_active' => 1]);
            $update->where(['user_uuid' => $current['user_uuid']]);
            return $this->updateWith($update) > 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     *
     * @param string $uuid
     * @param string $newHash
     * @return int
     */
    public function updatePassword(string $uuid, string $newHash): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_passwd' => $newHash, 'user_time_update' => time()]);
            $update->where(['user_uuid' => $uuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $code
     * @param string $newHash
     * @return int
     */
    public function updatePasswordWithForgotPasswdHash(string $code, string $newHash): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_passwd' => $newHash, 'user_time_update' => time()]);
            $update->where(['user_forgot_passwd_hash' => $code]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $uuid
     * @param int $active
     * @return int
     */
    public function updateActive(string $uuid, int $active): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_active' => $active, 'user_time_update' => time()]);
            $update->where(['user_uuid' => $uuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $uuid
     * @param string $email
     * @return int
     */
    public function updateEmail(string $uuid, string $email): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_email' => $email, 'user_time_update' => time()]);
            $update->where(['user_uuid' => $uuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userUuid
     * @return int
     */
    public function deleteUser(string $userUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_uuid' => $userUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @return int
     */
    public function countUsers(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['user_count' => new Expression('COUNT(user_uuid)')]);

            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $resultArr = $result->toArray();
                return $resultArr[0]['user_count'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param bool $onlyActive
     * @param bool $asKeyValObj
     * @return array
     */
    public function getUserUuidAssoc(bool $onlyActive = false, bool $asKeyValObj = false): array
    {
        $select = $this->sql->select();
        $uuidAssoc = [];
        try {
            if ($onlyActive) {
                $select->where(['user_active' => 1]);
            }
            $select->order('user_login');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    if(!$asKeyValObj) {
                        $uuidAssoc[$row['user_uuid']] = $row['user_login'] . ' (' . $row['user_email'] . ')';
                    } else {
                        $uuidAssoc[] = ['key' => $row['user_uuid'], 'val' => $row['user_login'] . ' (' . $row['user_email'] . ')'];
                    }
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $uuidAssoc;
    }

    /**
     * @param string $search
     * @param bool $onlyActive
     * @return array
     */
    public function searchUser(string $search, bool $onlyActive = false): array
    {
        $select = $this->sql->select();
        try {
            $select->where->like('user_login', '%' . $search . '%')->or->like('user_email', '%' . $search . '%');

            if ($onlyActive) {
                $select->where(['user_active' => 1]);
            }
            $select->columns(['user_uuid', 'user_login', 'user_time_create', 'user_email', 'user_lang_iso', 'user_time_update']);

            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);

            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $hash
     * @return bool
     */
    public function existPasswdForgotHash(string $hash): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_forgot_passwd_hash' => $hash]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function updateUserWithUserFormEntity(UserFormEntity $userFormEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'user_login'    => $userFormEntity->getUserLogin(),
                'user_active'   => $userFormEntity->getUserActive(),
                'user_email'    => $userFormEntity->getUserEmail(),
                'user_lang_iso' => $userFormEntity->getUserLangIso(),
            ]);
            $update->where(['user_uuid' => $userFormEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
