<?php

namespace Bitkorn\User\Controller\Admin\Ajax;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;

class UserSearchController extends AbstractUserController
{
    /**
     * @return JsonModel
     */
    public function searchSimpleAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $jsonModel->setArr($this->userService->searchUser((new FilterChainStringSanitize())->filter($request->getPost('search'))));
        return $jsonModel;
    }
}
