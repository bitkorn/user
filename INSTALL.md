
- copy [rightsnroles.local.dist.php](./data/app/config/autoload/rightsnroles.local.dist.php) to [App autoload config directory](../../../config/autoload/rightsnroles.local.php)

```postgresql
create type enum_supported_lang_iso as enum ('de', 'en');

alter type enum_supported_lang_iso owner to postgres;
```
