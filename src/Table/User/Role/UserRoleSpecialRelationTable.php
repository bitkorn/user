<?php

namespace Bitkorn\User\Table\User\Role;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class UserRoleSpecialRelationTable extends AbstractLibTable
{
    protected $table = 'user_role_special_relation';

    /**
     * @param string $userRoleSpecialRelationUuid
     * @return array
     */
    public function getUserRoleSpecialRelation(string $userRoleSpecialRelationUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_role_special_relation_uuid' => $userRoleSpecialRelationUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $resultArr = $result->toArray();
                return $resultArr[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $roleId
     * @return array
     */
    public function getUserRoleSpecialRelationsByRoleId(int $roleId): array
    {
        $select = $this->sql->select();
        try {

            $select->join('user_role_special', 'user_role_special.user_role_special_uuid = user_role_special_relation.user_role_special_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('user_role', 'user_role.user_role_id = user_role_special_relation.user_role_id', Select::SQL_STAR, Select::JOIN_LEFT);

            $select->order('user_role.user_role_id ASC, user_role_special.user_role_special_order_priority DESC');
            $select->where(['user_role_special_relation.user_role_id' => $roleId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * Only God!
     *
     * @return array
     */
    public function getUserRoleSpecialRelationsAllComplete(): array
    {
        $select = $this->sql->select();
        try {
            $select->join('user_role_special', 'user_role_special.user_role_special_uuid = user_role_special_relation.user_role_special_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('user_role', 'user_role.user_role_id = user_role_special_relation.user_role_id', Select::SQL_STAR, Select::JOIN_LEFT);

            $select->order('user_role.user_role_id ASC, user_role_special.user_role_special_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $roleSpecialUuid
     * @param int $roleId
     * @param int $roleSpecialRelationValue
     * @return string
     */
    public function insertUserRoleSpecialRelation(string $roleSpecialUuid, int $roleId, int $roleSpecialRelationValue = 0): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'user_role_special_relation_uuid'  => $uuid,
                'user_role_special_uuid'           => $roleSpecialUuid,
                'user_role_id'                     => $roleId,
                'user_role_special_relation_value' => $roleSpecialRelationValue
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $roleSpecialUuid
     * @return bool
     */
    public function deleteUserRoleSpecialRelationByRoleSpecialUuid(string $roleSpecialUuid): bool
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_role_special_uuid' => $roleSpecialUuid]);
            if ($this->deleteWith($delete) > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $userRoleSpecialRelationUuid
     * @param int $userRoleSpecialRelationValue
     * @return int
     */
    public function updateUserRoleSpecialRelationValue(string $userRoleSpecialRelationUuid, int $userRoleSpecialRelationValue): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['user_role_special_relation_value' => $userRoleSpecialRelationValue]);
            $update->where(['user_role_special_relation_uuid' => $userRoleSpecialRelationUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
