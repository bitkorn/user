<?php

namespace Bitkorn\User\Factory\Controller\Admin\Ajax;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\User\Controller\Admin\Ajax\UserManagementController;
use Bitkorn\User\Service\UserService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserManagementControllerFactory implements FactoryInterface
{

    /**
     * Create an UserManagementController
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return UserManagementController
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new UserManagementController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        return $controller;
    }
}
