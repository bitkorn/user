<?php

namespace Bitkorn\User\Controller\Ajax\Access;

use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\User\Service\RightsnrolesRoutesService;
use Bitkorn\User\Service\UserService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Validator\UsernameValidator;
use Laminas\Http\Header\SetCookie;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;

class AuthenticationController extends AbstractUserController
{

    protected int $sessionLifetime;
    protected bool $cookiesEnabled;
    protected RightsnrolesRoutesService $rightsnrolesRoutesService;

    public function setSessionLifetime(int $sessionLifetime): void
    {
        $this->sessionLifetime = $sessionLifetime;
    }

    public function setCookiesEnabled(bool $cookiesEnabled): void
    {
        $this->cookiesEnabled = $cookiesEnabled;
    }

    public function setRightsnrolesRoutesService(RightsnrolesRoutesService $rightsnrolesRoutesService): void
    {
        $this->rightsnrolesRoutesService = $rightsnrolesRoutesService;
    }

    /**
     * # POST /login
     *
     * Check login and response with session hash.
     *
     * **Body**: **x-www-form-urlencoded**
     *     - **login**: login value
     *     - **passwd**: the password
     * @return JsonModel
     *```
     *     {
     *         "desc": "",
     *         "auth": 1,
     *         "success": 1,
     *         "data": {
     *                     "session_hash": "12345678976d17645143ae9743d01a598c865461c6cbd9d19d636c221a34eda1"
     *                 },
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: If available, the session hash to identify the user in the future.
     *     - **messages**: only in development mode. E.g. one or more messages from input validation
     * ### HTTP
     *     - 403 insufficient permissions (min user role 10)
     *     - 400 not all values are valid
     *     - 500 error
     */
    public function loginAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if (!$request instanceof Request || !$response instanceof Response || !$request->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        $login = filter_input(INPUT_POST, 'login', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $password = filter_input(INPUT_POST, 'passwd', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (!(new UsernameValidator())->isValid($login) || empty($login) || empty($password)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if (!$this->userService->loadWithLoginAndPassword($login, $password)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        if ($this->cookiesEnabled) {
            $cookie = new SetCookie(UserService::SESSION_KEY, $this->userService->getUserSessionHash(), $this->sessionLifetime, '/');
            $response->getHeaders()->addHeader($cookie);
        }

        $userStorage = $this->userService->getUserEntity()->getStorage();
        unset($userStorage['user_passwd']);
        $jsonModel->setSuccess(1);
        $jsonModel->setVariables([
            'session_hash'        => $this->userService->getUserSessionHash(),
            'rightsnroles'        => $this->userService->getUserEntity()->getRightsnRoles(),
            'rightsnroles_routes' => $this->rightsnrolesRoutesService->getRightsnroles(),
            'user_uuid'           => $this->userService->getUserUuid(),
            'user'                => $userStorage
        ]);
        return $jsonModel;
    }

    /**
     * # GET /logout
     *
     * Delete the session
     *
     * @return JsonModel One JSON object
     *```
     *     {
     *         "desc": "",
     *         "auth": 0,
     *         "success": 0,
     *         "data": [],
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: An empty JSON array
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 200 success
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function logoutAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->logout();
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Fetch actual data for current logged-in user and refresh session data with it.
     * Provides the new data to the client.
     * @return JsonModel
     */
    public function checkAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->userService->loadWithSession();
        $userStorage = $this->userService->getUserEntity()->getStorage();
        unset($userStorage['user_passwd']);
        $jsonModel->setSuccess(1);
        $jsonModel->setVariables([
            'session_hash'        => $this->userService->getUserSessionHash(),
            'rightsnroles'        => $this->userService->getUserEntity()->getRightsnRoles(),
            'rightsnroles_routes' => $this->rightsnrolesRoutesService->getRightsnroles(),
            'user_uuid'           => $this->userService->getUserUuid(),
            'user'                => $userStorage
        ]);
        return $jsonModel;
    }

}
