<?php

namespace Bitkorn\User\Controller\Admin\Rest\Group;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Bitkorn\User\Form\User\Group\UserGroupForm;
use Bitkorn\User\Service\Group\UserGroupService;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class UserGroupRestController extends AbstractUserRestController
{
    protected UserGroupService $userGroupService;
    protected UserGroupForm $userGroupForm;

    public function setUserGroupService(UserGroupService $userGroupService): void
    {
        $this->userGroupService = $userGroupService;
    }

    public function setUserGroupForm(UserGroupForm $userGroupForm): void
    {
        $this->userGroupForm = $userGroupForm;
    }

    /**
     * # GET /user-admin-group
     *
     * Gets a list of ALL userGroups from current session.
     *
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "arr": [
     *                     {
     *                         "user_group_uuid": "637f857b-e408-11e8-b764-7824afbed67e",
     *                         "user_group_name": "testgroup",
     *                         "user_group_desc": "only for test",
     *                         "user_group_default": "0",
     *                         "user_uuids_csv": ""
     *                     },
     *                     {
     *                         ...
     *                     }
     *                 ],
     *         "messages": []
     *     }
     *```
     *     - **success**: 1 if operation success, else 0
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 403 insufficient permissions (min user role 10)
     *     - 200 success
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!empty($userGroups = $this->userGroupService->getUserGroups())) {
            $jsonModel->setArr($userGroups);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * # POST /user-admin-group
     *
     * Create a new userGroup.
     *
     * @param array $data **x-www-form-urlencoded**
     *     - **user_group_name**: some name
     *     - **user_group_desc**: some desc
     *     - **user_group_default**: if 1 then replace the current user_group_default
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "data": {
     *                     "user_group_uuid": "12345678-17e4-11e9-8e49-7824afbed67e"
     *                 },
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: If available, the new UUID from the generated record
     *     - **messages**: only in development mode. E.g. one or more messages from input validation
     * ### HTTP
     *     - 403 insufficient permissions (min user role 1)
     *     - 201 success created
     *     - 400 not all values are valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->userGroupForm->setUserGroupUuidAvailable(false);
        $this->userGroupForm->init();
        $this->userGroupForm->setData($data);
        if (!$this->userGroupForm->isValid()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            $jsonModel->addMessages($this->userGroupForm->getMessages());
            return $jsonModel;
        }
        $userGroupUuid = $this->userGroupService->insertUserGroup($this->userGroupForm->getData());
        if (empty($userGroupUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
            $jsonModel->setVariable('user_group_uuid', $userGroupUuid);
        }
        return $jsonModel;
    }

    /**
     * # DELETE /user-admin-group/:uuid
     *
     * Delete a userGroup and everything that belongs to it (user_group_relation).
     *
     * @param string $id UserGroup UUID
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "data": [],
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: An empty JSON array.
     *     - **messages**: only in development mode.
     * ### HTTP
     *     - 403 insufficient permissions (min user role 1)
     *     - 200 success
     *     - 204 Data not found
     *     - 400 not all values are valid OR the alias isn't valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->userGroupService->deleteUserGroupComplete($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * # PUT /user-admin-group/:userGroupUuid
     *
     * Update one userGroup record
     *
     * @param string $id URL Parameter
     * @param array $data **x-www-form-urlencoded**
     *     - **user_group_name**: some name
     *     - **user_group_desc**: some desc
     *     - **user_group_default**: if 1 then replace the current user_group_default
     * @return JsonModel
     *```
     *     {
     *         "success": 1,
     *         "data": [],
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 1 if authentication success, else 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: An empty array
     *     - **messages**: only in development mode. E.g. one or more messages from input validation
     * ### HTTP
     *     - 403 insufficient permissions (min user role 1)
     *     - 200 success data update
     *     - 202 success but nothing to update
     *     - 204 Data not found
     *     - 400 not all values are valid
     *     - 500 error
     * #### Headers
     *     - **SESSIONHASH** from /login response
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->userGroupForm->init();
        $data['user_group_uuid'] = $id;
        $this->userGroupForm->setData($data);
        if (!$this->userGroupForm->isValid()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            $jsonModel->addMessages($this->userGroupForm->getMessages());
            return $jsonModel;
        }
        $userGroupData = $this->userGroupForm->getData();
        $result = $this->userGroupService->updateUserGroup($userGroupData);
        switch ($result) {
            case -1:
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
                break;
            case 0:
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_202);
                $jsonModel->setSuccess(1);
                break;
            case 1:
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_200);
                $jsonModel->setSuccess(1);
                break;
        }
        return $jsonModel;
    }
}
