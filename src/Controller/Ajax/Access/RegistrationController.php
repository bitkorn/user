<?php

namespace Bitkorn\User\Controller\Ajax\Access;

use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\User\Form\User\UserForm;
use Bitkorn\User\Service\Registration\UserRegistrationService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Validator\UsernameValidator;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;

class RegistrationController extends AbstractUserController
{

    protected array $configUser;
    protected UserForm $userForm;
    protected UserRegistrationService $userRegistrationService;

    public function setConfigUser(array $configUser): void
    {
        $this->configUser = $configUser;
    }

    public function setUserForm(UserForm $userForm): void
    {
        $this->userForm = $userForm;
    }

    public function setUserRegistrationService(UserRegistrationService $userRegistrationService): void
    {
        $this->userRegistrationService = $userRegistrationService;
    }

    /**
     * # POST /register
     *
     * Create a new user and send verify email.
     *
     * **Body**: **x-www-form-urlencoded**
     *     - **user_login**: foo
     *     - **user_email**: foo
     *     - **user_passwd**: 123456
     *     - **user_passwd_confirm**: 123456
     * @return JsonModel
     *```
     *     {
     *         "desc": "",
     *         "auth": 0,
     *         "success": 1,
     *         "data": {
     *                     "some_uuid": "12345678-17e4-11e9-8e49-7824afbed67e"
     *                 },
     *         "messages": []
     *     }
     *```
     *     - **desc**: Description only in development mode
     *     - **auth**: 0
     *     - **success**: 1 if operation success, else 0
     *     - **data**: ???
     *     - **messages**: only in development mode. E.g. one or more messages from input validation
     * ### HTTP
     *     - 400 not all values are valid
     *     - 409 Conflict: login or email already exist.
     *     - 201 success created
     *     - 500 error
     */
    public function registerAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->configUser['registration_enabled']) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_423);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request instanceof Request || !$request->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $login = filter_input(INPUT_POST, 'user_login', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $email = filter_input(INPUT_POST, 'user_email', FILTER_SANITIZE_EMAIL);
        $passwd1 = $_POST['user_passwd'];
        $passwd2 = $_POST['user_passwd_confirm'];
        $lang = filter_input(INPUT_POST, 'user_lang_iso', FILTER_SANITIZE_EMAIL);

        if (!(new UsernameValidator())->isValid($login)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_409);
            $jsonModel->addMessage('not a valid username');
            return $jsonModel;
        }
        if ($this->userService->existUserLogin($login) || $this->userService->existUserEmail($email)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_409);
            $jsonModel->addMessage('login or email allready exist');
            return $jsonModel;
        }
        if ($passwd1 != $passwd2) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            $jsonModel->addMessage('password not equal');
            return $jsonModel;
        }

        $this->userForm->setUserPasswdAvailable(true);
        $this->userForm->setUserRoleIdAvailable(true);
        $this->userForm->init();
        $this->userForm->setData([
            'user_role_id'        => $this->configUser['user_role_id_default'],
            'user_login'          => $login,
            'user_passwd'         => $passwd1,
            'user_passwd_confirm' => $passwd2,
            'user_active'         => 0,
            'user_email'          => $email,
            'user_lang_iso'       => $lang
        ]);
        if (!$this->userForm->isValid()) {
            $jsonModel->addMessages($this->userForm->getMessages());
            return $jsonModel;
        }
        if ($this->userRegistrationService->registerUser($this->userForm->getData())) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function registerVerifyAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->configUser['registration_enabled']) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_423);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request instanceof Request || !$request->isGet()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        $registerHash = $this->params()->fromQuery('hash');
        if (!$this->userRegistrationService->verifyRegistration($registerHash)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
        } else {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function passwordForgotRequestCodeAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $request = $this->getRequest();
        if (!$request instanceof Request || !$request->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $userLogin = filter_var($request->getPost('user_login', ''), FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $userEmail = filter_var($request->getPost('user_email', ''), FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (empty($userLogin) && empty($userEmail)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->userRegistrationService->passwordForgotSendCode($userLogin, $userEmail)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        } else {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function passwordRenewWithCodeAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $request = $this->getRequest();
        if (!$request instanceof Request || !$request->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $code = filter_var($request->getPost('code', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $passwd1 = $request->getPost('passwd1', '');
        $passwd2 = $request->getPost('passwd2', '');
        if (empty($code) || empty($passwd1) || $passwd1 != $passwd2) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->userService->updateUserPasswdWithCode($code, $passwd1)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

}
